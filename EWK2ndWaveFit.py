################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kRed,kBlue,kGreen,kSpring,kOrange,kYellow,kWhite,kPink,kGray,kMagenta,kAzure,kDashed
from ROOT import TColor
import ROOT
from configWriter import Measurement,Sample
from systematic import Systematic
from copy import deepcopy
import subprocess
from SystematicsUtils import appendIfMatchName
from math import exp
from os import sys, path
import fnmatch
import EWKtheory

from SignalAccUncertainties import SignalTheoryUncertainties

from logger import Logger
log = Logger('EWK2ndWaveFit - 1L2L combination')

# ********************************************************************* #
#                              Helper functions
# ********************************************************************* #

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList[oldList.index(oldWeight)] = newWeight
    return newList

def addWeight(oldList,newWeight):
    newList = deepcopy(oldList)
    newList.append(newWeight)
    return newList

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def appendTo(item, toList):
    for to in toList:
        to.append(item)
    return item

def SetupChannels(channels, bkgFiles, systList):
    for chan in channels:
        for sample in chan.sampleList:
            if doBackupCacheFile == False:
                if sample.name != "data" and not "C1N2_WZ" in sample.name and not "C1C1_WW" in sample.name:
                    if "diboson" in sample.name: sample.addInputs(bkgFiles["diboson"])
                    else: sample.addInputs(bkgFiles[sample.name])
        for syst in systList:
            chan.addSystematic(syst)
    return

# ********************************************************************* #
#                              Configuration settings
# ********************************************************************* #

# check if we are on lxplus or not - useful to see when to load input trees from eos or from a local directory
onLxplus = 'lx' in subprocess   .getstatusoutput("hostname")[1] or 'vm' in subprocess   .getstatusoutput("hostname")[1] or '.cern.ch' in subprocess   .getstatusoutput("hostname")[1]

debug = False #!!!

if debug: 
    print ("WARNING: Systematics disabled for testing purposes!!!")

# here we have the possibility to activate different groups of systematic uncertainties
SystList = []

SystList.append("JER")                  # Jet Energy Resolution (common)
SystList.append("JES")                  # Jey Energy Scale (common)
SystList.append("MET")                  # MET (common)
SystList.append("LEP")                  # lepton uncertainties (common)
SystList.append("LepEff")               # lepton scale factors (common)
SystList.append("JVT")                  # JVT scale factors (common)
SystList.append("pileup")               # pileup (common)
SystList.append("BTag")                 # b-tagging uncertainties
SystList.append("diboson")              # scale variation for renormalization, factorization, resummation and generator comparison
#SystList.append("multiboson")           # scale variation for renormalization, factorization, resummation and generator comparison
SystList.append("wjets")                # scale variation for renormalization, factorization, resummation, CKKW and generator comparison
SystList.append("ttbar")                # Radiation and QCD scales, Hadronization/fracmentation, Hard scattering generation
SystList.append("singletop")            # Radiation and QCD scales, Hadronization/fracmentation, Hard scattering generation
SystList.append("zjets")                # scale variation for renormalization, factorization, resummation, matching
SystList.append("ttv")                  # part of ttv uncertainties
SystList.append("tth")                  # part of tth uncertainties
SystList.append("vh")                   # part of vh uncertainties
SystList.append("BSTag")                # boson tagging sf uncertainties

if debug: 
    SystList = []

#MJ: Remove syst for zjets in HMSR. py theo files also need to modify (comment zjets theo uncertainty in HMSR). 
doRemovezjetssystHMSR = True

# for creating the backup cache files, we do not necessarily want to have signal in - if flag true no signal included
doHistoBuilding = False

# for creating histograms in the backup cache file, we could run -t option to create histograms of backgrounds and data in all regions for both analyses
doBackupCacheFile = False

# Toggle N-1 plots
doNMinus1Plots = False

# Use reduced JES systematics
useReducedJESNP = False

# Use N Jet normalization factor
useNJetNormFac = False

# disable missing JES systematic for signal only
#disable_JES_PunchThrough_MC15_for_signal = True

# always use the CRs matching to a certain SR and run the associated tower containing SR and CRs
CRregions = ["resolved", "boosted"]                 # default - modify from command line

# activate inclusive signal regions for model-dependent fit and deactivate exclusive SRs: incl = True
# note that for inclusive SRs you cannot run more than one at a time (they are not orthogonal), still to get similar normalization factors as for the exclusion SRs, the same exclusive SRs are run
# for the VRs, the inclusive SRs are automatically enabled at the same time as the exclusive SRs are run
#incl = False

# Tower selected from command-line
# pickedSRs is set by the "-r" HisFitter option
try:
    pickedSRs
except NameError:
    pickedSRs = None

if pickedSRs != None and len(pickedSRs) >= 1:
    CRregions = pickedSRs
    print ("\n Tower defined from command line: ", pickedSRs, "      (-r resolved,boosted option)")
CRregions = CRregions + ["general"]

# activate associated validation regions 
ValidRegList = {}
# for plotting (turn to True if you want to use them):
ValidRegList["resolved"] = False
ValidRegList["boosted"] = False
ValidRegList["general"] = False

# for tables (turn doTableInputs to True)
doTableInputs = False    # This is used for bkg fit incl VR tables!  # This effectively means no validation plots but only validation tables (100x faster)

# N-1 plots in SRs
VRplots = False

# whether using same normfactor for ttbar and st
OneNFTopST = False


if myFitType == FitType.Exclusion:
    doTableInputs = False ## no need VR tables when exclusion fit


for cr in CRregions:
    if "resolved" in cr and doTableInputs:
        ValidRegList["resolved"] = True
    if "boosted" in cr and doTableInputs:
        ValidRegList["boosted"] = True
    if "general" in cr and doTableInputs:
        ValidRegList["general"] = True


# choose which kind of fit you want to perform: ShapeFit (True) or NoShapeFit (False)
#doShapeFit = True          #

# choose which analysis you want to work with: C1N2_WZ or C1C1_WW using e.g -u ana_C1N2_WZ
myAna = "C1N2_WZ"
if configMgr.userArg != "":
    if "ana" in configMgr.userArg: 
        myAna = configMgr.userArg.replace("ana_", "")

# take signal points from command line with -g and set only a default here:
if not 'sigSamples' in dir():
    if myAna == "C1N2_WZ":
        #if not debug: sigSamples = ["C1N2_WZ_300_0"]
        #else: 
        #sigSamples = ["C1N2_WZ_200_0", "C1N2_WZ_200_50", "C1N2_WZ_300_0", "C1N2_WZ_300_100", "C1N2_WZ_300_150", "C1N2_WZ_400_0", "C1N2_WZ_400_150", "C1N2_WZ_400_250", "C1N2_WZ_400_50", "C1N2_WZ_500_0", "C1N2_WZ_500_100", "C1N2_WZ_500_200", "C1N2_WZ_500_300", "C1N2_WZ_600_0", "C1N2_WZ_600_100", "C1N2_WZ_600_200", "C1N2_WZ_600_300", "C1N2_WZ_600_400", "C1N2_WZ_700_0", "C1N2_WZ_700_100", "C1N2_WZ_700_200", "C1N2_WZ_700_300", "C1N2_WZ_700_400", "C1N2_WZ_700_500", "C1N2_WZ_800_0", "C1N2_WZ_800_100", "C1N2_WZ_800_200", "C1N2_WZ_800_300", "C1N2_WZ_800_400", "C1N2_WZ_800_500", "C1N2_WZ_900p0_0p0", "C1N2_WZ_900_100", "C1N2_WZ_900p0_200p0", "C1N2_WZ_900_300", "C1N2_WZ_900p0_400p0", "C1N2_WZ_900_500", "C1N2_WZ_1000_0", "C1N2_WZ_1000_100", "C1N2_WZ_1000_200", "C1N2_WZ_1000_300", "C1N2_WZ_1000_400", "C1N2_WZ_1000_500", "C1N2_WZ_1100_0", "C1N2_WZ_1100_100", "C1N2_WZ_1100_200", "C1N2_WZ_1100_300", "C1N2_WZ_1100_400", "C1N2_WZ_1100_500", "C1N2_WZ_1200_0", "C1N2_WZ_1200_100", "C1N2_WZ_1200_200", "C1N2_WZ_1200_300", "C1N2_WZ_1200_400", "C1N2_WZ_500_250", "C1N2_WZ_600_250", "C1N2_WZ_650_250", "C1N2_WZ_700_250"] #removed 750_250, 850_250
        sigSamples = ["C1N2_WZ_200_50"]
    if myAna == "C1C1_WW":
        if not debug: sigSamples = ["C1C1_WW_300_0"]
        else: sigSamples = ["C1C1_WW_200_0", "C1C1_WW_200_50", "C1C1_WW_300_0", "C1C1_WW_300_100", "C1C1_WW_300_150", "C1C1_WW_400_0", "C1C1_WW_400_150", "C1C1_WW_400_250", "C1C1_WW_400_50", "C1C1_WW_500_0", "C1C1_WW_500_100", "C1C1_WW_500_200", "C1C1_WW_500_300", "C1C1_WW_600_0", "C1C1_WW_600_100", "C1C1_WW_600_200", "C1C1_WW_600_300", "C1C1_WW_600_400", "C1C1_WW_700_0", "C1C1_WW_700_100", "C1C1_WW_700_200", "C1C1_WW_700_300", "C1C1_WW_700_400", "C1C1_WW_700_500", "C1C1_WW_800_0", "C1C1_WW_800_100", "C1C1_WW_800_200", "C1C1_WW_800_300", "C1C1_WW_800_400", "C1C1_WW_800_500", "C1C1_WW_900p0_0p0", "C1C1_WW_900_100", "C1C1_WW_900p0_200p0", "C1C1_WW_900_300", "C1C1_WW_900p0_400p0", "C1C1_WW_900_500", "C1C1_WW_1000_0", "C1C1_WW_1000_100", "C1C1_WW_1000_200", "C1C1_WW_1000_300", "C1C1_WW_1000_400", "C1C1_WW_1000_500", "C1C1_WW_1100_0", "C1C1_WW_1100_100", "C1C1_WW_1100_200", "C1C1_WW_1100_300", "C1C1_WW_1100_400", "C1C1_WW_1100_500", "C1C1_WW_1200_0", "C1C1_WW_1200_100", "C1C1_WW_1200_200", "C1C1_WW_1200_300", "C1C1_WW_1200_400"]


#### dol MJ
print ('dol check: sigSamples[0] = ', sigSamples[0])
if "C1C1_WW" in sigSamples[0]:
    myAna = "C1C1_WW"
if "C1N2_WZ" in sigSamples[0]:
    myAna = "C1N2_WZ"


# define the analysis name: 
analysissuffix = ""

if not doBackupCacheFile:
    if myFitType == FitType.Exclusion:
        analysissuffix += "_" + sigSamples[0]
    else:
        if myAna == "C1N2_WZ": analysissuffix += "_C1N2_WZ"
        if myAna == "C1C1_WW": analysissuffix += "_C1C1_WW"

for cr in CRregions:
    analysissuffix += "_"
    analysissuffix += cr

analysissuffix_BackupCache = analysissuffix

if myFitType == FitType.Exclusion:
    analysissuffix += "_excl"


mylumi = 138.96516

mysamples = ["diboson", "multiboson", "singletop", "ttbar", "tth", "ttv", "vh", "wjets_Sh_2211", "zjets_Sh_2211", "data"]

doOnlySignal = False

# check for a user argument given with -u
myoptions = configMgr.userArg
analysisextension = ""
if doBackupCacheFile: analysisextension += "_backup"
if debug: analysisextension += "_NoSys"
if myoptions != "":
    if 'sensitivity' in myoptions:          # userArg should be something like 'sensitivity_3'
        mylumi = float(myoptions.split('_')[-1])
        analysisextension += "_" + myoptions
    
    if myoptions == 'doOnlySignal':
        doOnlySignal = True
        analysisextension += "_onlysignal_"+sigSamples[0]

    if 'samples' in myoptions:               # userArg should be something like samples_ttbar_ttv
        mysamples = []
        for sam in myoptions.split('_'):
            if sam != 'samples':
                mysamples.append(sam)
        analysisextension += "_" + myoptions
else:
    print ("No additional user arguments given - proceed with default analysis!")

print ("Using lumi: ", mylumi)

# First define HistFactory attributes
configMgr.analysisName = "EWK2ndWaveFit" + analysissuffix + analysisextension + "_v102"
configMgr.outputFileName = "results/" + configMgr.analysisName + ".root"
configMgr.histCacheFile = configMgr.analysisName + ".root"

# activate using of background histogram cache file to speed up processes
#if myoptions != "":
if onLxplus and not myFitType == FitType.Discovery:
    doBackupCacheFile = False #!!!
if doBackupCacheFile:
    configMgr.useCacheToTreeFallback = True       # enable the fallback to trees
    configMgr.useHistBackupCacheFile = doBackupCacheFile       # enable the use of an alternate data file
    histBackupCacheFileName = "EWK2ndWaveFit_" + myAna + analysissuffix_BackupCache + "_v102.root"
    print (" using backup cache file: " + histBackupCacheFileName)
    configMgr.histBackupCacheFile = histBackupCacheFileName


# Scaling calculated by outputLumi/inputLumi
configMgr.inputLumi = 0.001             # input lumi 1 pb^-1 = normalization of the HistFitter trees
configMgr.outputLumi = mylumi           # for test
configMgr.setLumiUnits("fb-1")          # Setting fb-1 here means that we do not need to add an additional scale factor of 1000, but use a scale factor of 1

configMgr.fixSigXSec = True

if debug: configMgr.fixSigXSec = False


useToys = False     #!!!
if useToys:
    configMgr.calculatorType = 0            # frequentist calculator (use toys)
    configMgr.nTOYs = 10000                 # number of toys when using frequentist calculator
    print (" using frequentist calculator (toys)")
else:
    configMgr.calculatorType = 2            # asymptotic calculator (creates asimov data set for the background hypothesis)
    print (" using asymptotic calculator")
configMgr.testStatType = 3                  # one-sided profile likelihood test statistics
configMgr.nPoints = 20                      # number of values scanned of signal-strength for upper-limit determination of signal strength

#configMgr.scanRange = (0.,10) #if you want to tune the range in a upper limit scan by hand

# writing xml files for bebugging purposes
configMgr.writeXML = True
if debug: configMgr.writeXML = False 


# blinding of various regions
configMgr.blindSR = False           # Blind the SRs (default is False)
configMgr.blindCR = False           # Blind the CRs (default is False)
configMgr.blindVR = False           # Blind the VRs (default is False)
doBlindSRinBGfit =  False           # Blind SR when performing a bkg fit

if debug:
    configMgr.blindSR = False           # Blind the SRs (default is False)
    configMgr.blindCR = False           # Blind the CRs (default is False)
    configMgr.blindVR = False           # Blind the VRs (default is False)
    doBlindSRinBGfit =  False           # Blind SR when performing a bkg fit

#useSignalInBlindedData = True
configMgr.ReduceCorrMatrix = True

#configMgr.prun = True
#configMgr.prunThreshold = 0.02

# using of statistical uncertainties
useStat = True

# Replacement of AF2 JES systematics will not be applied for these fullsim signal samples:
#FullSimSig = []

# FastSim signals use JET_JER_DataVsMC_AFII and JET_PunchThrough_AFII, while other FullSim samples used JET_JER_DataVsMC_MC16 and JET_PunchThrough_MC16.
# Here, the 2 latter will be added firstly for all samples, then they will be replaced by the 2 former later for FastSim signals, which are listed below
FastSimSig = ["C1N2_WZ_200_50", "C1N2_WZ_300_100", "C1N2_WZ_300_150", "C1N2_WZ_400_150", "C1N2_WZ_400_250", "C1N2_WZ_500_300", "C1N2_WZ_600_400", "C1N2_WZ_700_500", "C1C1_WW_200_0", "C1C1_WW_200_50", "C1C1_WW_300_100", "C1C1_WW_300_150", "C1C1_WW_400_150", "C1C1_WW_400_250", "C1C1_WW_500_300", "C1C1_WW_600_400"]


# ********************************************************************* #
#                              Location of HistFitter trees
# ********************************************************************* #

inputDir = "/gpfs_data/local/atlas/ballaben/1L/skimmed_with_JET_LargeR_JER/"

# Bkg files
bkgFiles_e = {}           # for electron channel
bkgFiles_m = {}           # for muon channel
bkgFiles_em = {}          # for both
for sam in mysamples:
    if ((sam != "data") and (sam != "wjets_Sh_2211") and (sam != "zjets_Sh_2211")):
        if doBackupCacheFile == False:
            bkgFiles_e[sam] = [inputDir + "allTrees_bkg_skimmed_new.root"]
            bkgFiles_m[sam] = [inputDir + "allTrees_bkg_skimmed_new.root"]
            bkgFiles_em[sam] = [inputDir + "allTrees_bkg_skimmed_new.root"]

if doBackupCacheFile == False:
    bkgFiles_e["wjets_Sh_2211"] = [inputDir + "Vjets_Sh_2211_skimmed_new.root"]
    bkgFiles_m["wjets_Sh_2211"] = [inputDir + "Vjets_Sh_2211_skimmed_new.root"]
    bkgFiles_em["wjets_Sh_2211"] = [inputDir + "Vjets_Sh_2211_skimmed_new.root"]

    bkgFiles_e["zjets_Sh_2211"] = [inputDir + "Vjets_Sh_2211_skimmed_new.root"]
    bkgFiles_m["zjets_Sh_2211"] = [inputDir+ "Vjets_Sh_2211_skimmed_new.root"]
    bkgFiles_em["zjets_Sh_2211"] = [inputDir + "Vjets_Sh_2211_skimmed_new.root"]

# signal files
if myFitType == FitType.Exclusion or doOnlySignal:
    sigFiles_e = {}
    sigFiles_m = {}
    sigFiles_em = {}
    for sigpoint in sigSamples:
        ## dol MJ
        if myAna == "C1N2_WZ":
            sigFiles_e[sigpoint] = [inputDir + "C1N2_WZ_skimmed_new.root"]
            sigFiles_m[sigpoint] = [inputDir + "C1N2_WZ_skimmed_new.root"]
            sigFiles_em[sigpoint] = [inputDir + "C1N2_WZ_skimmed_new.root"]
        if myAna == "C1C1_WW":
            sigFiles_e[sigpoint] = [inputDir + "C1C1_WW_skimmed_new.root"]
            sigFiles_m[sigpoint] = [inputDir + "C1C1_WW_skimmed_new.root"]
            sigFiles_em[sigpoint] = [inputDir + "C1C1_WW_skimmed_new.root"]


# data files
if doBackupCacheFile == False:
    dataFiles = [inputDir + "allTrees_data_NoSys.root"]




# ********************************************************************* #	
#                              Regions
# ********************************************************************* #

# 1l Preselection
preSelection_1l = "nLep_base==1 && nLep_signal==1 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1 && nLep_combiBase<3 && nLep_combiBaseHighPt<2"

# 2l selection
preSelection_2l = "nLep_base==2 && nLep_signal==2 && nJet30>=1 && nJet30<=3 && met>200. && mt>50. && trigMatch_singleLepTrig==1"

# SRs
# resolved
resolvedSRSelection = "lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.6 && mjj>70. && mjj<105. && nFatjets==0"
configMgr.cutsDict["SRLMresolved"] = preSelection_1l + "&&" + resolvedSRSelection + "&& mt>200. && mt<300."
configMgr.cutsDict["SRHMresolved"] = preSelection_1l + "&&" + resolvedSRSelection + "&& mt>300."
# boosted for C1N2_WZ analysis
boostedSRSelection_WZ = "lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Ztagged==1 && fatjet1Pt>250. && met_Signif>12. && ((mjj>80. && mjj<100. && meffInc30<850.) || (meffInc30>850.))"
configMgr.cutsDict["SRLMboostedWZ"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>120. && mt<200."
configMgr.cutsDict["SRMMboostedWZ"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>200. && mt<300."
configMgr.cutsDict["SRHMboostedWZ"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>300."
configMgr.cutsDict["SRHMboostedN1mtWZ"] = preSelection_1l + "&&" + boostedSRSelection_WZ 


# boosted for C1C1_WW analysis
boostedSRSelection_WW = "lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && ((met_Signif>12. && mjj>70. && mjj<90. && meffInc30<850.) || (met_Signif>15. && meffInc30>850.))"
configMgr.cutsDict["SRLMboostedWW"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>120. && mt<200."
configMgr.cutsDict["SRMMboostedWW"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>200. && mt<300."
configMgr.cutsDict["SRHMboostedWW"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>300."


#discovery region
configMgr.cutsDict["SRLMboostedWZdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6&& nFatjets>=1  && fatjet1Ztagged==1 && fatjet1Pt>250. && met_Signif>12. && (mjj>80. && mjj<100.)&&mt>120&&mt<200 && meffInc30>600."
configMgr.cutsDict["SRMMboostedWZdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Ztagged==1 && fatjet1Pt>250. && met_Signif>12. &&mt>200&& mt<300 && meffInc30>850."
configMgr.cutsDict["SRHMboostedWZdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Ztagged==1 && fatjet1Pt>250. && met_Signif>12. && mt>300 && meffInc30>850."

configMgr.cutsDict["SRLMboostedWWdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && met_Signif>15. &&mt>120&&mt<200 && meffInc30>600."
configMgr.cutsDict["SRMMboostedWWdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && met_Signif>15. &&mt>200&& mt<300 && meffInc30>600."
configMgr.cutsDict["SRHMboostedWWdisc"] = preSelection_1l + "&& lep1Pt>25. && nJet30>=1 && nJet30<=3 && met>200. && dPhi_lep_met<2.6 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250. && met_Signif>12. && mjj>70. && mjj<90 && mt>300 && meffInc30>850."



# DB2L CR/VR
DB2LSelection = "lep1Pt>25. && lep2Pt>25. && nJet30>=1 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.9 && mll>70. && mll<100. && (mjj<75. || mjj>95.)"

configMgr.cutsDict["DB2LCR"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>12. && mt>50. && mt<200."
configMgr.cutsDict["DB2LVR"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>10. && mt>200. && mt<350."

# WDB1L boosted CR/VR
WDB1LBoostedSelection = "lep1Pt>25. && nJet30>=1 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.9 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250."
LowMeff = " && meffInc30>600 && meffInc30<850"
HighMeff = " && meffInc30>850" 
configMgr.cutsDict["WDB1LCRboosted"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>50. && mt<80."
configMgr.cutsDict["WDB1LVR1boosted"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>80."
configMgr.cutsDict["WDB1LVR2boosted"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120."

configMgr.cutsDict["WDB1LCRboostedmTLowMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12." + "&& mt>50. && mt<80." + LowMeff
configMgr.cutsDict["WDB1LCRboostedmTHighMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12." + "&&  mt>50. && mt<80." + HighMeff
configMgr.cutsDict["WDB1LCRboostedMETSigLowMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12." + "&& mt>50. && mt<80." + LowMeff
configMgr.cutsDict["WDB1LCRboostedMETSigHighMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12." + "&&  mt>50. && mt<80." + HighMeff
configMgr.cutsDict["WDB1LCRboostedAllMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12." + "&& mt>50. && mt<80."

configMgr.cutsDict["WDB1LVR1boostedmTLowMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>80." + LowMeff
configMgr.cutsDict["WDB1LVR1boostedmTHighMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>80." + HighMeff
configMgr.cutsDict["WDB1LVR1boostedMETSigLowMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>80." + LowMeff
configMgr.cutsDict["WDB1LVR1boostedMETSigHighMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>80." + HighMeff
configMgr.cutsDict["WDB1LVR1boostedAllMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif<12. && mt>80."

configMgr.cutsDict["WDB1LVR2boostedmTLowMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120." + LowMeff
configMgr.cutsDict["WDB1LVR2boostedmTHighMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120." + HighMeff
configMgr.cutsDict["WDB1LVR2boostedMETSigLowMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120." + LowMeff
configMgr.cutsDict["WDB1LVR2boostedMETSigHighMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120." + HighMeff
configMgr.cutsDict["WDB1LVR2boostedAllMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120."


configMgr.cutsDict["DB2LCRmeffInc30"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>12. && mt>50. && mt<200."
configMgr.cutsDict["DB2LCRMETSig"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>12. && mt>50. && mt<200."
configMgr.cutsDict["DB2LCRmT"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>12. && mt>50. && mt<200."

configMgr.cutsDict["DB2LVRmeffInc30"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>10. && mt>200. && mt<350."
configMgr.cutsDict["DB2LVRMETSig"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>10. && mt>200. && mt<350."
configMgr.cutsDict["DB2LVRmT"] = preSelection_2l + "&&" + DB2LSelection + "&& met_Signif>10. && mt>200. && mt<350."




configMgr.cutsDict["WDB1LCRVR2boostedMETSigHighMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&& ((met_Signif<12." + "&&  mt>50. && mt<80.) || (met_Signif>12. && mt>50. && mt<120.))"  + HighMeff
configMgr.cutsDict["WDB1LCRVR1boostedmTLowMeff"] = preSelection_1l + "&&" + WDB1LBoostedSelection + "&&(( met_Signif<12. && mt>80)|| (met_Signif<12." + "&& mt>50. && mt<80.))" + LowMeff





# WDB1L resolved CR/VR
WDB1LResolvedSelection = "lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1==0 && met>200. && dPhi_lep_met<2.8 && mjj>70. && mjj<105. && nFatjets==0 && met_Signif>12."
configMgr.cutsDict["WDB1LCRresolved"] = preSelection_1l + "&&" + WDB1LResolvedSelection + "&& mt>50. && mt<80."
configMgr.cutsDict["WDB1LVRresolved"] = preSelection_1l + "&&" + WDB1LResolvedSelection + "&& mt>80. && mt<200."

# Top boosted CR/VR
TBoostedSelection = "lep1Pt>25. && nJet30>=1 && nJet30<=3 && nBJet30_DL1>0 && met>200. && dPhi_lep_met<2.9 && nFatjets>=1 && fatjet1Wtagged==1 && fatjet1Pt>250."
configMgr.cutsDict["TCRboosted"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12 && mt>50. && mt<80."
configMgr.cutsDict["TVR1boosted"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12. && mt>80."
configMgr.cutsDict["TVR2boosted"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120."

# Top resolved CR/VR
TResolvedSelection = "lep1Pt>25. && nJet30>=2 && nJet30<=3 && nBJet30_DL1>0 && met>200. && dPhi_lep_met<2.8 && mjj>70. && mjj<105. && nFatjets==0"
configMgr.cutsDict["TCRresolved"] = preSelection_1l + "&&" + TResolvedSelection + "&& mt>50. && mt<80."
configMgr.cutsDict["TVRresolved"] = preSelection_1l + "&&" + TResolvedSelection + "&& mt>80. && mt<200."




configMgr.cutsDict["TCRboostedmTLowMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12." + "&& mt>50. && mt<80." + LowMeff
configMgr.cutsDict["TCRboostedmTHighMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12." + "&&  mt>50. && mt<80." + HighMeff
configMgr.cutsDict["TCRboostedMETSigLowMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12." + "&& mt>50. && mt<80." + LowMeff
configMgr.cutsDict["TCRboostedMETSigHighMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12." + "&&  mt>50. && mt<80." + HighMeff
configMgr.cutsDict["TCRboostedAllMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12." + "&& mt>50. && mt<80."

configMgr.cutsDict["TboostedVR1mTLowMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12. && mt>80." + LowMeff
configMgr.cutsDict["TboostedVR1mTHighMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12. && mt>80." + HighMeff
configMgr.cutsDict["TboostedVR1METSigLowMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12. && mt>80." + LowMeff
configMgr.cutsDict["TboostedVR1METSigHighMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12. && mt>80." + HighMeff
configMgr.cutsDict["TboostedVR1AllMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif<12. && mt>80."

configMgr.cutsDict["TboostedVR2mTLowMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120." + LowMeff
configMgr.cutsDict["TboostedVR2mTHighMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120." + HighMeff
configMgr.cutsDict["TboostedVR2METSigLowMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120." + LowMeff
configMgr.cutsDict["TboostedVR2METSigHighMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120." + HighMeff
configMgr.cutsDict["TboostedVR2AllMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& met_Signif>12. && mt>50. && mt<120."

configMgr.cutsDict["SRLMboostedWWMeff"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>120. && mt<200."
configMgr.cutsDict["SRMMboostedWWMeff"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>200. && mt<300."
configMgr.cutsDict["SRHMboostedWWMeff"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>300."

configMgr.cutsDict["SRLMboostedWZMeff"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>120. && mt<200."
configMgr.cutsDict["SRMMboostedWZMeff"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>200. && mt<300."
configMgr.cutsDict["SRHMboostedWZMeff"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>300."

configMgr.cutsDict["SRboostedWZmT"] = preSelection_1l + "&&" + boostedSRSelection_WZ + "&& mt>120"
configMgr.cutsDict["SRboostedWWmT"] = preSelection_1l + "&&" + boostedSRSelection_WW + "&& mt>120"


configMgr.cutsDict["TboostedTCRVR2METSigHighMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& ((met_Signif>12. && mt>50. && mt<120.)||(met_Signif<12." + "&&  mt>50. && mt<80.))" + HighMeff
configMgr.cutsDict["TboostedTCRVR1mTLowMeff"] = preSelection_1l + "&&" + TBoostedSelection + "&& ((met_Signif<12. && mt>80.)||(met_Signif<12." + "&& mt>50. && mt<80.))" + LowMeff


d = configMgr.cutsDict
defined_regions = []
if "resolved" in CRregions:
    defined_regions += ["WDB1LCRresolved", "TCRresolved", "WDB1LVRresolved", "TVRresolved", "SRLMresolved", "SRHMresolved"]
if "boosted" in CRregions:
    defined_regions += ["TboostedVR2METSigHighMeff","TboostedVR1mTLowMeff","TCRboostedMETSigHighMeff","TCRboostedmTLowMeff","WDB1LVR2boostedMETSigHighMeff","WDB1LVR1boostedmTLowMeff","WDB1LCRboostedMETSigHighMeff","WDB1LCRboostedmTLowMeff","TboostedTCRVR1mTLowMeff","TboostedTCRVR2METSigHighMeff","WDB1LCRVR1boostedmTLowMeff","WDB1LCRVR2boostedMETSigHighMeff","WDB1LCRboosted", "TCRboosted", "WDB1LVR1boosted", "WDB1LVR2boosted", "TVR1boosted", "TVR2boosted","WDB1LCRboostedmTHighMeff","WDB1LCRboostedMETSigLowMeff","WDB1LCRboostedAllMeff","WDB1LVR1boostedmTHighMeff","WDB1LVR1boostedMETSigLowMeff","WDB1LVR1boostedMETSigHighMeff","WDB1LVR1boostedAllMeff","WDB1LVR2boostedmTLowMeff","WDB1LVR2boostedmTHighMeff","WDB1LVR2boostedMETSigLowMeff","WDB1LVR2boostedAllMeff","TCRboostedmTHighMeff","TCRboostedMETSigLowMeff","TCRboostedAllMeff","TboostedVR1mTHighMeff","TboostedVR1METSigLowMeff","TboostedVR1METSigHighMeff","TboostedVR1AllMeff","TboostedVR2mTLowMeff","TboostedVR2mTHighMeff","TboostedVR2METSigLowMeff","TboostedVR2AllMeff","DB2LCRmeffInc30","DB2LCRMETSig","DB2LCRmT","DB2LVRmeffInc30","DB2LVRMETSig","DB2LVRmT","SRLMboostedWWMeff","SRMMboostedWWMeff","SRHMboostedWWMeff","SRLMboostedWZMeff","SRMMboostedWZMeff","SRHMboostedWZMeff","SRboostedWZmT","SRboostedWWmT"]
    if not doBackupCacheFile:
        if myAna == "C1N2_WZ":
            defined_regions += ["SRLMboostedWZ", "SRMMboostedWZ", "SRHMboostedWZ","SRLMboostedWZdisc", "SRMMboostedWZdisc","SRHMboostedWZdisc","SRHMboostedN1mtWZ"]
        if myAna == "C1C1_WW":
            defined_regions += ["SRLMboostedWW", "SRMMboostedWW", "SRHMboostedWW","SRLMboostedWWdisc", "SRMMboostedWWdisc","SRHMboostedWWdisc"]
    else:
        defined_regions += ["TboostedVR2METSigHighMeff","TboostedVR1mTLowMeff","TCRboostedMETSigHighMeff","TCRboostedmTLowMeff","WDB1LVR2boostedMETSigHighMeff","WDB1LVR1boostedmTLowMeff","WDB1LCRboostedMETSigHighMeff","WDB1LCRboostedmTLowMeff","TboostedTCRVR1mTLowMeff","TboostedTCRVR2METSigHighMeff","WDB1LCRVR1boostedmTLowMeff","WDB1LCRVR2boostedMETSigHighMeff","SRLMboostedWZ", "SRMMboostedWZ", "SRHMboostedWZ", "SRLMboostedWW", "SRMMboostedWW", "SRHMboostedWW", "SRLMboostedWZdisc", "SRMMboostedWZdisc","SRHMboostedWZdisc","SRLMboostedWWdisc", "SRMMboostedWWdisc","SRHMboostedWWdisc","SRHMboostedN1mtWZ","WDB1LCRboostedmTHighMeff","WDB1LCRboostedMETSigLowMeff","WDB1LCRboostedAllMeff","WDB1LVR1boostedmTHighMeff","WDB1LVR1boostedMETSigLowMeff","WDB1LVR1boostedMETSigHighMeff","WDB1LVR1boostedAllMeff","WDB1LVR2boostedmTLowMeff","WDB1LVR2boostedmTHighMeff","WDB1LVR2boostedMETSigLowMeff","WDB1LVR2boostedAllMeff","TCRboostedmTHighMeff","TCRboostedMETSigLowMeff","TCRboostedAllMeff","TboostedVR1mTHighMeff","TboostedVR1METSigLowMeff","TboostedVR1METSigHighMeff","TboostedVR1AllMeff","TboostedVR2mTLowMeff","TboostedVR2mTHighMeff","TboostedVR2METSigLowMeff","TboostedVR2AllMeff","DB2LCRmeffInc30","DB2LCRMETSig","DB2LCRmT","DB2LVRmeffInc30","DB2LVRMETSig","DB2LVRmT","SRLMboostedWWMeff","SRMMboostedWWMeff","SRHMboostedWWMeff","SRLMboostedWZMeff","SRMMboostedWZMeff","SRHMboostedWZMeff","SRboostedWZmT","SRboostedWWmT"]
if "general" in CRregions:
    defined_regions += ["DB2LCR", "DB2LVR"]

for pre_region in defined_regions:
    configMgr.cutsDict[pre_region + "El"] = d[pre_region] + "&& AnalysisType==1"
    configMgr.cutsDict[pre_region + "Mu"] = d[pre_region] + "&& AnalysisType==2"
    configMgr.cutsDict[pre_region + "EM"] = d[pre_region] + "&& (AnalysisType==1 || AnalysisType==2)"

# ********************************************************************* #
#                              Weights and systematics
# ********************************************************************* #

# all the weights we need for a default analysis - the boson tagging weights are added later
weights = ["genWeight", "eventWeight", "leptonWeight", "jvtWeight", "pileupWeight", "trigWeight_singleLepTrig", "polWeight", "bTagWeight"]
configMgr.weights = weights


# Systematics

# example on how to modify weights for systematic uncertainties
xsecSigHighWeights = replaceWeight(weights,"genWeight","genWeightUp")
xsecSigLowWeights = replaceWeight(weights,"genWeight","genWeightDown")

# muon related uncertainties acting on weights
# Bad muon
#muonEffBadMuonHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_BADMUON_SYS__1up")
#muonEffBadMuonLowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_BADMUON_SYS__1down")
# Iso
muonEffIsoHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_ISO_STAT__1up")
muonEffIsoLowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_ISO_STAT__1down")
muonEffIsoHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_ISO_SYS__1up")
muonEffIsoLowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_ISO_SYS__1down")
# Reco
muonEffRecoLowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_STAT__1down")
muonEffRecoHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_STAT__1up")
muonEffRecoLowWeights_stat_lowpt = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down")
muonEffRecoHighWeights_stat_lowpt = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up")
muonEffRecoLowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_SYS__1down")
muonEffRecoHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_SYS__1up")
muonEffRecoLowWeights_sys_lowpt = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down")
muonEffRecoHighWeights_sys_lowpt = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up")
# TTVA
muonEffTTVALowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TTVA_STAT__1down")
muonEffTTVAHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TTVA_STAT__1up")
muonEffTTVALowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TTVA_SYS__1down")
muonEffTTVAHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TTVA_SYS__1up")
# Trig
muonEffTrigLowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TrigStatUncertainty__1down")
muonEffTrigHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TrigStatUncertainty__1up")
muonEffTrigLowWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TrigSystUncertainty__1down")
muonEffTrigHighWeights_sys = replaceWeight(weights, "leptonWeight", "leptonWeight_MUON_EFF_TrigSystUncertainty__1up")

# electron related uncertainties acting on weights
# Charge id
#electronChargeIDLowWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_CHARGEID_STAT__1down")
#electronChargeIDHighWeights_stat = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_CHARGEID_STAT__1up")
# Charge id sel
#electronEffChargeIDSelLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down")
#electronEffChargeIDSelHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up")
# ID
electronEffIDLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffIDHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
# Iso
electronEffIsoLowWeights = replaceWeight(weights,"leptonWeight", "leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffIsoHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
# Reco
electronEffRecoLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffRecoHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
# Trigger
electronEffTrigEffLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffTrigEffHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up")
electronEffTrigLowWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down")
electronEffTrigHighWeights = replaceWeight(weights, "leptonWeight", "leptonWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up")

# JVT weight systematics
jvtEffLowWeights = replaceWeight(weights, "jvtWeight", "jvtWeight_JET_JvtEfficiency__1down")
jvtEffHighWeights = replaceWeight(weights, "jvtWeight", "jvtWeight_JET_JvtEfficiency__1up")

# pileup weight systematics
pileupLowWeights = replaceWeight(weights, "pileupWeight", "pileupWeightDown")
pileupHighWeights = replaceWeight(weights, "pileupWeight", "pileupWeightUp")

# b-tag weight systematics
bTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
bTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")

cTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
cTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")

mTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
mTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")

eTagHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
eTagLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")

eTagFromCHighWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1up")
eTagFromCLowWeights = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1down")

basicChanSyst = {}
elChanSyst = {}
muChanSyst = {}
bTagSyst = {}
cTagSyst = {}
mTagSyst = {}
eTagSyst = {}
eTagFromCSyst = {}
bsTagSFSyst = {}

for region in CRregions:
    basicChanSyst[region] = []
    elChanSyst[region] = []
    muChanSyst[region] = []
    bsTagSFSyst[region] = []

    # adding dummy 30% syst as an overallSys when debug
    # if debug: basicChanSyst[region].append(Systematic("dummy", configMgr.weights, 1.30, 0.70, "user", "userOverallSys"))

    # Trigger systematic uncertainty
    #basicChanSyst[region].append(Systematic("singleleptrigger", configMgr.weights, 1.02, 0.98, "user", "userOverallSys"))

    #Example systematic uncertainty
    if "JER" in SystList: 
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_1", "_NoSys", "_JET_JER_EffectiveNP_1__1up", "_JET_JER_EffectiveNP_1__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_2", "_NoSys", "_JET_JER_EffectiveNP_2__1up", "_JET_JER_EffectiveNP_2__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_3", "_NoSys", "_JET_JER_EffectiveNP_3__1up", "_JET_JER_EffectiveNP_3__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_4", "_NoSys", "_JET_JER_EffectiveNP_4__1up", "_JET_JER_EffectiveNP_4__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_5", "_NoSys", "_JET_JER_EffectiveNP_5__1up", "_JET_JER_EffectiveNP_5__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_6", "_NoSys", "_JET_JER_EffectiveNP_6__1up", "_JET_JER_EffectiveNP_6__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_7", "_NoSys", "_JET_JER_EffectiveNP_7__1up", "_JET_JER_EffectiveNP_7__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_8", "_NoSys", "_JET_JER_EffectiveNP_8__1up", "_JET_JER_EffectiveNP_8__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_9", "_NoSys", "_JET_JER_EffectiveNP_9__1up", "_JET_JER_EffectiveNP_9__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_10", "_NoSys", "_JET_JER_EffectiveNP_10__1up", "_JET_JER_EffectiveNP_10__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_11", "_NoSys", "_JET_JER_EffectiveNP_11__1up", "_JET_JER_EffectiveNP_11__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JER_EffectiveNP_12restTerm", "_NoSys", "_JET_JER_EffectiveNP_12restTerm__1up", "_JET_JER_EffectiveNP_12restTerm__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("JET_LargeR_JER", "_NoSys", "_JET_LargeR_JER", "_NoSys", "tree", "overallNormHistoSysOneSideSym"))
	# The syst below will be replaced later with AFII for FastSim signals
        basicChanSyst[region].append(Systematic("JER_DataVsMC_MC16", "_NoSys", "_JET_JER_DataVsMC_MC16__1up", "_JET_JER_DataVsMC_MC16__1down", "tree", "overallNormHistoSys"))

    if "JES" in SystList: 
        if useReducedJESNP :
            basicChanSyst[region].append(Systematic("JES_Group1", "_NoSys", "_JET_GroupedNP_1__1up", "_JET_GroupedNP_1__1down", "tree", "overallNormHistoSys"))    
            basicChanSyst[region].append(Systematic("JES_Group2", "_NoSys", "_JET_GroupedNP_2__1up", "_JET_GroupedNP_2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Group3", "_NoSys", "_JET_GroupedNP_3__1up", "_JET_GroupedNP_3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JET", "_NoSys", "_JET_EtaIntercalibration_NonClosure__1up", "_JET_EtaIntercalibration_NonClosure__1down","tree","overallNormHistoSys")) 
        else :
            basicChanSyst[region].append(Systematic("JES_BJES_Response", "_NoSys", "_JET_BJES_Response__1up", "_JET_BJES_Response__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Baseline", "_NoSys", "_JET_CombMass_Baseline__1up", "_JET_CombMass_Baseline__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Modelling", "_NoSys", "_JET_CombMass_Modelling__1up", "_JET_CombMass_Modelling__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_TotalStat", "_NoSys", "_JET_CombMass_TotalStat__1up", "_JET_CombMass_TotalStat__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Tracking1", "_NoSys", "_JET_CombMass_Tracking1__1up", "_JET_CombMass_Tracking1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Tracking2", "_NoSys", "_JET_CombMass_Tracking2__1up", "_JET_CombMass_Tracking2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_CombMass_Tracking3", "_NoSys", "_JET_CombMass_Tracking3__1up", "_JET_CombMass_Tracking3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Detector1", "_NoSys", "_JET_EffectiveNP_Detector1__1up", "_JET_EffectiveNP_Detector1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Detector2", "_NoSys", "_JET_EffectiveNP_Detector2__1up", "_JET_EffectiveNP_Detector2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Mixed1", "_NoSys", "_JET_EffectiveNP_Mixed1__1up", "_JET_EffectiveNP_Mixed1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Mixed2", "_NoSys", "_JET_EffectiveNP_Mixed2__1up", "_JET_EffectiveNP_Mixed2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Mixed3", "_NoSys", "_JET_EffectiveNP_Mixed3__1up", "_JET_EffectiveNP_Mixed3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Modelling1", "_NoSys", "_JET_EffectiveNP_Modelling1__1up", "_JET_EffectiveNP_Modelling1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Modelling2", "_NoSys", "_JET_EffectiveNP_Modelling2__1up", "_JET_EffectiveNP_Modelling2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Modelling3", "_NoSys", "_JET_EffectiveNP_Modelling3__1up", "_JET_EffectiveNP_Modelling3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Modelling4", "_NoSys", "_JET_EffectiveNP_Modelling4__1up", "_JET_EffectiveNP_Modelling4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Detector1", "_NoSys", "_JET_EffectiveNP_R10_Detector1__1up", "_JET_EffectiveNP_R10_Detector1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Detector2", "_NoSys", "_JET_EffectiveNP_R10_Detector2__1up", "_JET_EffectiveNP_R10_Detector2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Mixed1", "_NoSys", "_JET_EffectiveNP_R10_Mixed1__1up", "_JET_EffectiveNP_R10_Mixed1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Mixed2", "_NoSys", "_JET_EffectiveNP_R10_Mixed2__1up", "_JET_EffectiveNP_R10_Mixed2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Mixed3", "_NoSys", "_JET_EffectiveNP_R10_Mixed3__1up", "_JET_EffectiveNP_R10_Mixed3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Mixed4", "_NoSys", "_JET_EffectiveNP_R10_Mixed4__1up", "_JET_EffectiveNP_R10_Mixed4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Modelling1", "_NoSys", "_JET_EffectiveNP_R10_Modelling1__1up", "_JET_EffectiveNP_R10_Modelling1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Modelling2", "_NoSys", "_JET_EffectiveNP_R10_Modelling2__1up", "_JET_EffectiveNP_R10_Modelling2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Modelling3", "_NoSys", "_JET_EffectiveNP_R10_Modelling3__1up", "_JET_EffectiveNP_R10_Modelling3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Modelling4", "_NoSys", "_JET_EffectiveNP_R10_Modelling4__1up", "_JET_EffectiveNP_R10_Modelling4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical1", "_NoSys", "_JET_EffectiveNP_R10_Statistical1__1up", "_JET_EffectiveNP_R10_Statistical1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical2", "_NoSys", "_JET_EffectiveNP_R10_Statistical2__1up", "_JET_EffectiveNP_R10_Statistical2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical3", "_NoSys", "_JET_EffectiveNP_R10_Statistical3__1up", "_JET_EffectiveNP_R10_Statistical3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical4", "_NoSys", "_JET_EffectiveNP_R10_Statistical4__1up", "_JET_EffectiveNP_R10_Statistical4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical5", "_NoSys", "_JET_EffectiveNP_R10_Statistical5__1up", "_JET_EffectiveNP_R10_Statistical5__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_R10_Statistical6", "_NoSys", "_JET_EffectiveNP_R10_Statistical6__1up", "_JET_EffectiveNP_R10_Statistical6__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical1", "_NoSys", "_JET_EffectiveNP_Statistical1__1up", "_JET_EffectiveNP_Statistical1__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical2", "_NoSys", "_JET_EffectiveNP_Statistical2__1up", "_JET_EffectiveNP_Statistical2__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical3", "_NoSys", "_JET_EffectiveNP_Statistical3__1up", "_JET_EffectiveNP_Statistical3__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical4", "_NoSys", "_JET_EffectiveNP_Statistical4__1up", "_JET_EffectiveNP_Statistical4__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical5", "_NoSys", "_JET_EffectiveNP_Statistical5__1up", "_JET_EffectiveNP_Statistical5__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EffectiveNP_Statistical6", "_NoSys", "_JET_EffectiveNP_Statistical6__1up", "_JET_EffectiveNP_Statistical6__1down", "tree", "overallNormHistoSys")) 
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_Modelling", "_NoSys", "_JET_EtaIntercalibration_Modelling__1up", "_JET_EtaIntercalibration_Modelling__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure_2018data", "_NoSys", "_JET_EtaIntercalibration_NonClosure_2018data__1up", "_JET_EtaIntercalibration_NonClosure_2018data__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure_highE", "_NoSys", "_JET_EtaIntercalibration_NonClosure_highE__1up", "_JET_EtaIntercalibration_NonClosure_highE__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure_negEta", "_NoSys", "_JET_EtaIntercalibration_NonClosure_negEta__1up", "_JET_EtaIntercalibration_NonClosure_negEta__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_NonClosure_posEta", "_NoSys", "_JET_EtaIntercalibration_NonClosure_posEta__1up", "_JET_EtaIntercalibration_NonClosure_posEta__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_R10_TotalStat", "_NoSys", "_JET_EtaIntercalibration_R10_TotalStat__1up", "_JET_EtaIntercalibration_R10_TotalStat__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_EtaIntercalibration_TotalStat", "_NoSys", "_JET_EtaIntercalibration_TotalStat__1up", "_JET_EtaIntercalibration_TotalStat__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Flavor_Composition", "_NoSys", "_JET_Flavor_Composition__1up", "_JET_Flavor_Composition__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Flavor_Response", "_NoSys", "_JET_Flavor_Response__1up", "_JET_Flavor_Response__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_LargeR_TopologyUncertainty_top", "_NoSys", "_JET_LargeR_TopologyUncertainty_top__1up", "_JET_LargeR_TopologyUncertainty_top__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_LargeR_TopologyUncertainty_V", "_NoSys", "_JET_LargeR_TopologyUncertainty_V__1up", "_JET_LargeR_TopologyUncertainty_V__1down", "tree", "overallNormHistoSys"))            
            basicChanSyst[region].append(Systematic("JES_Pileup_OffsetMu", "_NoSys", "_JET_Pileup_OffsetMu__1up", "_JET_Pileup_OffsetMu__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Pileup_OffsetNPV", "_NoSys", "_JET_Pileup_OffsetNPV__1up", "_JET_Pileup_OffsetNPV__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Pileup_PtTerm", "_NoSys", "_JET_Pileup_PtTerm__1up", "_JET_Pileup_PtTerm__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_Pileup_RhoTopology", "_NoSys", "_JET_Pileup_RhoTopology__1up", "_JET_Pileup_RhoTopology__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_PunchThrough_MC16", "_NoSys", "_JET_PunchThrough_MC16__1up", "_JET_PunchThrough_MC16__1down", "tree", "overallNormHistoSys")) ## will be replaced with AFII for FastSim signals
            basicChanSyst[region].append(Systematic("JES_SingleParticle_HighPt", "_NoSys", "_JET_SingleParticle_HighPt__1up", "_JET_SingleParticle_HighPt__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_MassRes_Hbb_comb", "_NoSys", "_JET_MassRes_Hbb_comb__1up", "_JET_MassRes_Hbb_comb__1down", "tree", "overallNormHistoSysOneSideSym"))
            basicChanSyst[region].append(Systematic("JES_MassRes_Top_comb", "_NoSys", "_JET_MassRes_Top_comb__1up", "_JET_MassRes_Top_comb__1down", "tree", "overallNormHistoSysOneSideSym"))
            basicChanSyst[region].append(Systematic("JES_MassRes_WZ_comb", "_NoSys", "_JET_MassRes_WZ_comb__1up", "_JET_MassRes_WZ_comb__1down", "tree", "overallNormHistoSysOneSideSym"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_Dijet_Modelling", "_NoSys", "_JET_JetTagSF_Dijet_Modelling__1up", "_JET_JetTagSF_Dijet_Modelling__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_Gammajet_Modelling", "_NoSys", "_JET_JetTagSF_Gammajet_Modelling__1up", "_JET_JetTagSF_Gammajet_Modelling__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_Hadronisation", "_NoSys", "_JET_JetTagSF_Hadronisation__1up", "_JET_JetTagSF_Hadronisation__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_MatrixElement__1up", "_NoSys", "_JET_JetTagSF_MatrixElement__1up", "_JET_JetTagSF_MatrixElement__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_JetTagSF_Radiation", "_NoSys", "_JET_JetTagSF_Radiation__1up", "_JET_JetTagSF_Radiation__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_BGSF_Dijet_Stat", "_NoSys", "_JET_WTag_SigEff50_BGSF_Dijet_Stat__1up", "_JET_WTag_SigEff50_BGSF_Dijet_Stat__1up", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_BGSF_Gammajet_Stat", "_NoSys", "_JET_WTag_SigEff50_BGSF_Gammajet_Stat__1up", "_JET_WTag_SigEff50_BGSF_Gammajet_Stat__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_BGSF_Propagated_AllOthers", "_NoSys", "_JET_WTag_SigEff50_BGSF_Propagated_AllOthers__1up", "_JET_WTag_SigEff50_BGSF_Propagated_AllOthers__1up", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_BinVariation", "_NoSys", "_JET_WTag_SigEff50_SigSF_BinVariation__1up", "_JET_WTag_SigEff50_SigSF_BinVariation__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_ExtrapolationPt", "_NoSys", "_JET_WTag_SigEff50_SigSF_ExtrapolationPt__1up", "_JET_WTag_SigEff50_SigSF_ExtrapolationPt__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_ExtrapolationZ", "_NoSys", "_JET_WTag_SigEff50_SigSF_ExtrapolationZ__1up", "_JET_WTag_SigEff50_SigSF_ExtrapolationZ__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_Propagated_AllOthers", "_NoSys", "_JET_WTag_SigEff50_SigSF_Propagated_AllOthers__1up", "_JET_WTag_SigEff50_SigSF_Propagated_AllOthers__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_SigSF_Statistics", "_NoSys", "_JET_WTag_SigEff50_SigSF_Statistics__1up", "_JET_WTag_SigEff50_SigSF_Statistics__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_TagEffUnc_GlobalBackground", "_NoSys", "_JET_WTag_SigEff50_TagEffUnc_GlobalBackground__1up", "_JET_WTag_SigEff50_TagEffUnc_GlobalBackground__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_TagEffUnc_GlobalOther__1up", "_NoSys", "_JET_WTag_SigEff50_TagEffUnc_GlobalOther__1up", "_JET_WTag_SigEff50_TagEffUnc_GlobalOther__1down", "tree", "overallNormHistoSys"))
            basicChanSyst[region].append(Systematic("JES_WTag_SigEff50_TagEffUnc_GlobalSignal", "_NoSys", "_JET_WTag_SigEff50_TagEffUnc_GlobalSignal__1up", "_JET_WTag_SigEff50_TagEffUnc_GlobalSignal__1down", "tree", "overallNormHistoSys"))
	    


	        
    if "MET" in SystList:
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPara", "_NoSys", "_MET_SoftTrk_ResoPara", "_NoSys", "tree", "overallNormHistoSysOneSideSym"))        
        basicChanSyst[region].append(Systematic("MET_SoftTrk_ResoPerp", "_NoSys", "_MET_SoftTrk_ResoPerp", "_NoSys", "tree", "overallNormHistoSysOneSideSym"))
        basicChanSyst[region].append(Systematic("MET_SoftTrk_Scale", "_NoSys", "_MET_SoftTrk_Scale__1up", "_MET_SoftTrk_Scale__1down", "tree", "overallNormHistoSys"))

    if "LEP" in SystList:
        basicChanSyst[region].append(Systematic("EG_RESOLUTION_ALL", "_NoSys", "_EG_RESOLUTION_ALL__1up", "_EG_RESOLUTION_ALL__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("EG_SCALE_AF2", "_NoSys", "_EG_SCALE_AF2__1up", "_EG_SCALE_AF2__1down", "tree", "overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("EG_SCALE_ALL", "_NoSys", "_EG_SCALE_ALL__1up", "_EG_SCALE_ALL__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_ID", "_NoSys", "_MUON_ID__1up", "_MUON_ID__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_MS", "_NoSys", "_MUON_MS__1up", "_MUON_MS__1down", "tree", "overallNormHistoSys"))        
        basicChanSyst[region].append(Systematic("MUON_SAGITTA_RESBIAS","_NoSys", "_MUON_SAGITTA_RESBIAS__1up", "_MUON_SAGITTA_RESBIAS__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_SAGITTA_RHO", "_NoSys", "_MUON_SAGITTA_RHO__1up", "_MUON_SAGITTA_RHO__1down", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_SCALE", "_NoSys", "_MUON_SCALE__1up", "_MUON_SCALE__1down", "tree", "overallNormHistoSys"))

    if "LepEff" in SystList:
        basicChanSyst[region].append(Systematic("MUON_EFF_ISO_STAT",configMgr.weights, muonEffIsoHighWeights_stat, muonEffIsoLowWeights_stat, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_ISO_SYS", configMgr.weights, muonEffIsoHighWeights_sys, muonEffIsoLowWeights_sys, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_RECO_STAT", configMgr.weights, muonEffRecoHighWeights_stat, muonEffRecoLowWeights_stat, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_RECO_STAT_LOWPT", configMgr.weights, muonEffRecoHighWeights_stat_lowpt, muonEffRecoLowWeights_stat_lowpt, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_RECO_SYS", configMgr.weights, muonEffRecoHighWeights_sys, muonEffRecoLowWeights_sys, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_RECO_SYS_LOWPT", configMgr.weights, muonEffRecoHighWeights_sys_lowpt, muonEffRecoLowWeights_sys_lowpt, "weight", "overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("MUON_TTVA_STAT", configMgr.weights, muonEffTTVAHighWeights_stat, muonEffTTVALowWeights_stat, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_TTVA_SYS", configMgr.weights, muonEffTTVAHighWeights_sys, muonEffTTVALowWeights_sys, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_TrigStatUncertainty", configMgr.weights, muonEffTrigHighWeights_stat, muonEffTrigLowWeights_stat, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("MUON_EFF_TrigSystUncertainty", configMgr.weights, muonEffTrigHighWeights_sys, muonEffTrigLowWeights_sys, "weight", "overallNormHistoSys"))
        #basicChanSyst[region].append(Systematic("MUON_Eff_BadMuon_sys", configMgr.weights, muonEffBadMuonHighWeights_sys, muonEffBadMuonLowWeights_sys, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("EG_EFF_ID", configMgr.weights, electronEffIDHighWeights, electronEffIDLowWeights, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("EG_EFF_Iso", configMgr.weights, electronEffIsoHighWeights, electronEffIsoLowWeights, "weight", "overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_EFF_Reco", configMgr.weights, electronEffRecoHighWeights, electronEffRecoLowWeights, "weight", "overallNormHistoSys")) 
        basicChanSyst[region].append(Systematic("EG_EFF_TriggerEff", configMgr.weights, electronEffTrigEffHighWeights, electronEffTrigEffLowWeights, "weight", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("EG_EFF_Trigger",configMgr.weights, electronEffTrigHighWeights, electronEffTrigLowWeights, "weight", "overallNormHistoSys")) 
    if "JVT" in SystList:
        basicChanSyst[region].append(Systematic("JvtEfficiency", configMgr.weights, jvtEffHighWeights, jvtEffLowWeights, "weight", "overallNormHistoSys")) 

    if "pileup" in SystList:
        basicChanSyst[region].append(Systematic("Pileup", configMgr.weights, pileupHighWeights, pileupLowWeights, "weight", "overallNormHistoSys"))

    if "BTag" in SystList:
        bTagSyst[region] = Systematic("btag_BT", configMgr.weights, bTagHighWeights, bTagLowWeights, "weight", "overallNormHistoSys")
        cTagSyst[region] = Systematic("btag_CT", configMgr.weights, cTagHighWeights, cTagLowWeights, "weight", "overallNormHistoSys")
        mTagSyst[region] = Systematic("btag_LightT", configMgr.weights, mTagHighWeights, mTagLowWeights, "weight", "overallNormHistoSys")
        eTagSyst[region] = Systematic("btag_Extra", configMgr.weights, eTagHighWeights, eTagLowWeights, "weight", "overallNormHistoSys")
        eTagFromCSyst[region] = Systematic("btag_ExtraFromCharm", configMgr.weights, eTagFromCHighWeights, eTagFromCLowWeights, "weight", "overallNormHistoSys")

    if "BSTag" in SystList:
        basicChanSyst[region].append(Systematic("BSTag_B_0", "_NoSys", "_bTag_B_0__1down","_bTag_B_0__1up", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("BSTag_Light_0", "_NoSys", "_bTag_Light_0__1down","_bTag_Light_0__1up", "tree", "overallNormHistoSys"))
        basicChanSyst[region].append(Systematic("BSTag_Light_1", "_NoSys", "_bTag_Light_1__1down","_bTag_Light_1__1up", "tree", "overallNormHistoSys"))

        
# signal uncertainties
xsecSig = Systematic("SigXSec", configMgr.weights, xsecSigHighWeights, xsecSigLowWeights, "weight", "overallSys")
    
# Generator Systematics
generatorSyst = []
    
if "ttbar" in SystList and "ttbar" in mysamples:    
    # ttbar uncertainties
    import theoryUncertainties_EWK2ndWave_ttbar
    theoryUncertainties_EWK2ndWave_ttbar.TheorUnc(generatorSyst)

if "singletop" in SystList and "singletop" in mysamples:
    # single top uncertainties
    import theoryUncertainties_EWK2ndWave_singletop
    theoryUncertainties_EWK2ndWave_singletop.TheorUnc(generatorSyst) 
    
if "diboson" in SystList and "diboson" in mysamples:
    # diboson uncertainties
    import theoryUncertainties_EWK2ndWave_diboson2l
    theoryUncertainties_EWK2ndWave_diboson2l.TheorUnc(generatorSyst)
    import theoryUncertainties_EWK2ndWave_diboson1l
    theoryUncertainties_EWK2ndWave_diboson1l.TheorUnc(generatorSyst)

if "multiboson" in SystList and "multiboson" in mysamples:
    # multiboson uncertainties
    import theoryUncertainties_EWK2ndWave_multiboson
    theoryUncertainties_EWK2ndWave_multiboson.TheorUnc(generatorSyst)        

if "wjets" in SystList and "wjets_Sh_2211" in mysamples:
    # wjets uncertainties
    import theoryUncertainties_EWK2ndWave_wjets_Sh_2211
    theoryUncertainties_EWK2ndWave_wjets_Sh_2211.TheorUnc(generatorSyst) 

if "zjets" in SystList and "zjets_Sh_2211" in mysamples:
    # zjets uncertainties
    import theoryUncertainties_EWK2ndWave_zjets_Sh_2211
    theoryUncertainties_EWK2ndWave_zjets_Sh_2211.TheorUnc(generatorSyst)
    
    
if "ttv" in SystList and "ttv" in mysamples:
    #ttv uncertainties
    import theoryUncertainties_EWK2ndWave_ttv
    theoryUncertainties_EWK2ndWave_ttv.TheorUnc(generatorSyst)

if "tth" in SystList and "tth" in mysamples:
    #tth uncertainties
    import theoryUncertainties_EWK2ndWave_tth
    theoryUncertainties_EWK2ndWave_tth.TheorUnc(generatorSyst)

if "vh" in SystList and "vh" in mysamples:
    #vh uncertainties
    import theoryUncertainties_EWK2ndWave_vh
    theoryUncertainties_EWK2ndWave_vh.TheorUnc(generatorSyst)

# ********************************************************************* #
#                              Samples
# ********************************************************************* #

configMgr.nomName = "_NoSys"

# Bkg samples
WSampleName = "wjets_Sh_2211"
WSample = Sample(WSampleName, kAzure-4)
WSample.setNormFactor("mu_WDB1L", 1., 0., 5.)
WSample.setStatConfig(useStat)

TtbarSampleName = "ttbar"
TtbarSample = Sample(TtbarSampleName, kGreen-9)
TtbarSample.setNormFactor("mu_Top", 1., 0., 5.)
TtbarSample.setStatConfig(useStat)

Diboson1LSampleName = "diboson1l"
Diboson1LSample = Sample(Diboson1LSampleName, kOrange-8)
Diboson1LSample.setNormFactor("mu_WDB1L", 1., 0., 5.)
Diboson1LSample.setStatConfig(useStat)
Diboson1LSample.setPrefixTreeName("diboson")
Diboson1LSample.addSampleSpecificWeight("(DatasetNumber==364255 || DatasetNumber==363359 || DatasetNumber==363360 || DatasetNumber==363489 || DatasetNumber==364304 || DatasetNumber==364305)")

Diboson2LSampleName = "diboson2l"
Diboson2LSample = Sample(Diboson2LSampleName, kOrange-8)
Diboson2LSample.setNormFactor("mu_DB2L", 1., 0., 5.)
Diboson2LSample.setStatConfig(useStat)
Diboson2LSample.setPrefixTreeName("diboson")
Diboson2LSample.addSampleSpecificWeight("(DatasetNumber==364250 || DatasetNumber==364253 || DatasetNumber==364254 || DatasetNumber==364286 || DatasetNumber==364288 || DatasetNumber==364289 || DatasetNumber==364290 || DatasetNumber==363356 || DatasetNumber==363358 || DatasetNumber==364283 || DatasetNumber==364284 || DatasetNumber==364285 || DatasetNumber==364287 || DatasetNumber==364302 || DatasetNumber==345708 || DatasetNumber==345709 || DatasetNumber==345716 || DatasetNumber==345720 || DatasetNumber==345725)")

Diboson0LSampleName = "diboson0l"
Diboson0LSample = Sample(Diboson0LSampleName, kOrange-8)
Diboson0LSample.setStatConfig(useStat)
Diboson0LSample.setNormByTheory()
Diboson0LSample.setPrefixTreeName("diboson")
Diboson0LSample.addSampleSpecificWeight("(DatasetNumber==363494 || DatasetNumber==363355 || DatasetNumber==363357 || DatasetNumber==364303)")

if OneNFTopST:
    SingleTopSampleName = "singletop"
    SingleTopSample = Sample(SingleTopSampleName, kGreen-5)
    SingleTopSample.setNormFactor("mu_Top", 1., 0., 5.)
    SingleTopSample.setStatConfig(useStat)
else:	
    SingleTopSampleName = "singletop"
    SingleTopSample = Sample(SingleTopSampleName, kGreen-5)
    SingleTopSample.setStatConfig(useStat)
    SingleTopSample.setNormByTheory()




ZSampleName = "zjets_Sh_2211"
ZSample = Sample(ZSampleName, kBlue+3)
ZSample.setStatConfig(useStat)
#ZSample.setNormFactor("mu_Zjets", 1., 0., 5.)
#ZSample.setNormFactor("mu_Zyjets", 1., 0., 5.)
#ZSample.setNormByTheory()

TtbarVSampleName = "ttv"
TtbarVSample = Sample(TtbarVSampleName, kGreen-8)
TtbarVSample.setStatConfig(useStat)
TtbarVSample.setNormByTheory()

TtbarHSampleName = "tth"
TtbarHSample = Sample(TtbarHSampleName, kGreen-8)
TtbarHSample.setStatConfig(useStat)
TtbarHSample.setNormByTheory()

TribosonSampleName = "multiboson"
TribosonSample = Sample(TribosonSampleName, kOrange-8)
TribosonSample.setStatConfig(useStat)
TribosonSample.setNormByTheory()

VHSampleName = "vh"
VHSample = Sample(VHSampleName, kGreen-8)
VHSample.setStatConfig(useStat)
VHSample.setNormByTheory()

# data sample for later
DataSampleName = "data"
DataSample = Sample(DataSampleName, kBlack)
if doBackupCacheFile == False:
    DataSample.addInputs(dataFiles)
DataSample.setData()


if doOnlySignal:
    SignalSampleName = sigSamples[0].replace("p5", "").replace("p0", "")
    SignalSample = Sample(SignalSampleName, kPink)
    SignalSample.setPrefixTreeName(sigSamples[0])
    SignalSample.setStatConfig(useStat)
    SignalSample.setNormByTheory()
    SignalSample.setNormFactor("mu_SIG", 1., 0., 5.)
    SignalSample.addInputs(sigFiles_em[sigSamples[0]])



# ******* 2L2Jets ******* #
InputDir = '/gpfs_data/local/atlas/ballaben/2L2J/2Ljets_v1_8/'
RegionSkimDir = '/gpfs_data/local/atlas/ballaben/2L2J/2Ljets_v1_8_hadd/'


'''
configMgr.cutsDict["CRVZ"] = "trigMatch_2LTrig && nJet30 >= 1 && lepPt[1] > 25. && nLep_signal == 2 && nLep_combi == 2 && lepCharge[0] != lepCharge[1] && met_Et > 100. && met_Sign > 6. && 12. < mll && mll < 111. && lepFlavor[0] == lepFlavor[1] && nBJet20_MV2c10_FixedCutBEff_77 == 0 && 81. < mll && mll < 101. && 12. < met_Sign && met_Sign < 18. && mt2leplsp_0 > 80. && nJet30 >= 2 && !(60. < mjj && mjj < 110.) && mjj > 20."

configMgr.cutsDict["CRWZ"] = "trigMatch_2LTrig && nJet30 >= 2 && lepPt[1] > 25. && nLep_signal == 2 && nLep_combi == 2 && lepCharge[0] == lepCharge[1] && nBJet20_MV2c10_FixedCutBEff_77 == 0 && 12. < mll && met_Sign > 6. && met_Et > 100."

configMgr.cutsDict["CRtt"] = "trigMatch_2LTrig && nJet30 >= 1 && lepPt[1] > 25. && nLep_signal == 2 && nLep_combi == 2 && lepCharge[0] != lepCharge[1] && met_Et > 100. && met_Sign > 6. && 12. < mll && mll < 111. && lepFlavor[0] == lepFlavor[1] && nBJet20_MV2c10_FixedCutBEff_77 >= 1 && 81. < mll && mll < 101. && 9. < met_Sign && met_Sign < 12. && mt2leplsp_0 > 80. && jetPt[0] > 60. && nJet30 >= 2 && mjj > 20."

configMgr.cutsDict["CRZ"] = "trigMatch_2LTrig && nJet30 >= 1 && lepPt[1] > 25. && nLep_signal == 2 && nLep_combi == 2 && lepCharge[0] != lepCharge[1] && met_Et > 100. && met_Sign > 6. && 12. < mll && mll < 111. && lepFlavor[0] == lepFlavor[1] && nBJet20_MV2c10_FixedCutBEff_77 == 0 && 81. < mll && mll < 101. && 6. < met_Sign && met_Sign < 9. && mt2leplsp_0 > 80. && nJet30 == 2 && !(60. < mjj && mjj < 110.) && mjj > 20. && Rll < 1."

configMgr.cutsDict["CRDY"] = "trigMatch_2LTrig && nJet30 >= 1 && lepPt[1] > 25. && nLep_signal == 2 && nLep_combi == 2 && lepCharge[0] != lepCharge[1] && met_Et > 100. && met_Sign > 6. && 12. < mll && mll < 111. && lepFlavor[0] == lepFlavor[1] && nBJet20_MV2c10_FixedCutBEff_77 == 0 && 12. < mll && mll < 71. && 6 < met_Sign && met_Sign < 9. && mt2leplsp_0 > 100. && nJet30 >= 2"
'''

#https://gitlab.cern.ch/atlas-phys-susy-2LJetsLegacy/SusySkim2LJetsLegacy/-/blob/master/scripts/EWKSkimmers/regions.h
configMgr.cutsDict["CRVZ"] = "regionID == 201"
configMgr.cutsDict["CRtt"] = "regionID == 202"
configMgr.cutsDict["CRZ"] = "regionID == 203"
configMgr.cutsDict["CRDY"] = "regionID == 204"


normRegions2L2J = [("CRVZ", "cuts"),("CRtt", "cuts"),("CRZ", "cuts"),("CRDY","cuts")]


myDarkBlue = TColor.GetColor("#08306b")
myMediumPink = TColor.GetColor("#fcc5c0")
myLightOrange = TColor.GetColor("#fec49f")
myMediumOrange = TColor.GetColor("#fe9929")
myMediumGreen = TColor.GetColor("#41ab5d")
myDarkGreen = TColor.GetColor("#006d2c")
myLightBlue = TColor.GetColor("#9ecae1")
myMediumBlue = myMediumBlue = TColor.GetColor("#0868ac")
myMediumYellow = TColor.GetColor("#FFFB00")



DataSampleName2L2J = "data"
DataSample2L2J = Sample(DataSampleName2L2J, kBlack)
DataSample2L2J.addInputs([RegionSkimDir+'DATA_mc16a.root'])
DataSample2L2J.addInputs([RegionSkimDir+'DATA_mc16cd.root'])
DataSample2L2J.addInputs([RegionSkimDir+'DATA_mc16e.root'])
DataSample2L2J.setData()


#0.00000719604 = 1/(1000*mylumi)
ttbar2L2JName = "ttbar"
ttbar2L2J = Sample(ttbar2L2JName, myDarkBlue)
#ttbar2L2J.setNormByTheory()
ttbar2L2J.setNormFactor("mu_Top", 1., 0., 5.)
ttbar2L2J.setNormRegions(normRegions2L2J)
ttbar2L2J.setStatConfig(useStat)
ttbar2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
ttbar2L2J.addInputs([RegionSkimDir+'BKGS_mc16a.root'])
ttbar2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd.root'])
ttbar2L2J.addInputs([RegionSkimDir+'BKGS_mc16e.root'])



WSample2L2JName = "Wjets"
WSample2L2J = Sample(WSample2L2JName, kAzure-4)
WSample2L2J.setNormByTheory()
WSample2L2J.setStatConfig(useStat)
WSample2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
WSample2L2J.addInputs([RegionSkimDir+'BKGS_mc16a.root'])
WSample2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd.root'])
WSample2L2J.addInputs([RegionSkimDir+'BKGS_mc16e.root'])


Higgs2L2JName = "higgs"
Higgs2L2J = Sample(Higgs2L2JName, myMediumPink)
Higgs2L2J.setNormByTheory()
Higgs2L2J.setStatConfig(useStat)
Higgs2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
Higgs2L2J.addInputs([RegionSkimDir+'BKGS_mc16a.root'])
Higgs2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd.root'])
Higgs2L2J.addInputs([RegionSkimDir+'BKGS_mc16e.root'])


TribosonName2L2J = "triboson"
Triboson2L2J = Sample(TribosonName2L2J, myLightOrange)
Triboson2L2J.setNormByTheory()
Triboson2L2J.setStatConfig(useStat)
Triboson2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
Triboson2L2J.addInputs([RegionSkimDir+'BKGS_mc16a.root'])
Triboson2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd.root'])
Triboson2L2J.addInputs([RegionSkimDir+'BKGS_mc16e.root'])


DibosonName2L2J = "diboson2l"
Diboson2L2J = Sample(DibosonName2L2J, myMediumOrange)
#Diboson2L2J.setNormByTheory()
Diboson2L2J.setNormFactor("mu_DB2L", 1., 0., 5.)
Diboson2L2J.setNormRegions(normRegions2L2J)
Diboson2L2J.setStatConfig(useStat)
Diboson2L2J.setPrefixTreeName("diboson")
Diboson2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
Diboson2L2J.addInputs([RegionSkimDir+'BKGS_mc16a.root'])
Diboson2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd.root'])
Diboson2L2J.addInputs([RegionSkimDir+'BKGS_mc16e.root'])


ZjetsName2L2J = "Zjets"
Zjets2L2J = Sample(ZjetsName2L2J, myMediumGreen)
#Zjets2L2J.setNormByTheory()
Zjets2L2J.setNormFactor("mu_Zjets", 1., 0., 5.)
Zjets2L2J.setNormRegions(normRegions2L2J)
Zjets2L2J.setStatConfig(useStat)
Zjets2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
Zjets2L2J.addInputs([RegionSkimDir+'BKGS_mc16a.root'])
Zjets2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd.root'])
Zjets2L2J.addInputs([RegionSkimDir+'BKGS_mc16e.root'])


ZyjetsName2L2J = "Zyjets"
Zyjets2L2J = Sample(ZyjetsName2L2J, myDarkGreen)
Zyjets2L2J.setOverrideTreename("Zjets")
#Zyjets2L2J.setNormByTheory()
Zyjets2L2J.setNormFactor("mu_Zyjets", 1., 0., 5.)
Zyjets2L2J.setNormRegions(normRegions2L2J)
Zyjets2L2J.setStatConfig(useStat)
Zyjets2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
Zyjets2L2J.addInputs([RegionSkimDir+'BKGS_mc16a_Zy.root'])
Zyjets2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd_Zy.root'])
Zyjets2L2J.addInputs([RegionSkimDir+'BKGS_mc16e_Zy.root'])


topOtherName2L2J = "topOther"
topOther2L2J = Sample(topOtherName2L2J, myLightBlue)
topOther2L2J.setNormByTheory()
topOther2L2J.setStatConfig(useStat)
topOther2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
topOther2L2J.addInputs([RegionSkimDir+'BKGS_mc16a.root'])
topOther2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd.root'])
topOther2L2J.addInputs([RegionSkimDir+'BKGS_mc16e.root'])


singleTopName2L2J = "singleTop"
singleTop2L2J = Sample(singleTopName2L2J, myMediumBlue)
singleTop2L2J.setNormByTheory()
singleTop2L2J.setStatConfig(useStat)
singleTop2L2J.setWeights(["0.00000719604","AllWeight","AllPR"])
singleTop2L2J.addInputs([RegionSkimDir+'BKGS_mc16a.root'])
singleTop2L2J.addInputs([RegionSkimDir+'BKGS_mc16cd.root'])
singleTop2L2J.addInputs([RegionSkimDir+'BKGS_mc16e.root'])


FNPName2L2J = "FNP"
FNP2L2J = Sample(FNPName2L2J, myMediumYellow)
FNP2L2J.setOverrideTreename("MM")
FNP2L2J.setSuffixTreeName("_CENTRAL")
FNP2L2J.addSampleSpecificWeight("FNP_WEIGHTS")
FNP2L2J.setNormByTheory()
FNP2L2J.setStatConfig(useStat)
FNP2L2J.setWeights(["0.00000719604"]) #add AllWeight if you run without regionID
FNP2L2J.addInputs([RegionSkimDir+'FAKES_mc16a.root'])
FNP2L2J.addInputs([RegionSkimDir+'FAKES_mc16cd.root'])
FNP2L2J.addInputs([RegionSkimDir+'FAKES_mc16e.root'])


samplelist2L2J = [DataSample2L2J,ttbar2L2J,WSample2L2J,Higgs2L2J,Triboson2L2J,Diboson2L2J,Zjets2L2J,Zyjets2L2J,topOther2L2J,singleTop2L2J,FNP2L2J]



sysFull = {"Zjets": Zjets2L2J,
"Zyjets" : Zyjets2L2J,
"diboson" : Diboson2L2J,
"ttbar" : ttbar2L2J,
"topOther" : topOther2L2J,
"singleTop" : singleTop2L2J
}

normBkgs = ["Zjets","Zyjets","ttbar","diboson"]

from collections import defaultdict
booked_samp_systs = defaultdict(list)


def addSys(name, nominal=None, up=None, down=None, stype="",method="histoSys"):
    """ Book systematics to be added on a channel basis. """
    # find a shorter label
    label = name
    replacements = {
        "JET_EffectiveNP_": "JES_EffectiveNP_",
        "JET_JER_EffectiveNP_": "JER_EffectiveNP_",
        "JET_JER_": "JER_",
        "JET_BJES_": "JES_BJES_",
        "JET_EtaIntercalibration_": "JES_EtaIntercalibration_",
        "JET_Flavor_": "JES_Flavor_",
        "JET_Pileup_": "JES_Pileup_",
        "JET_PunchThrough_": "JES_PunchThrough_",
        "JET_SingleParticle": "JES_SingleParticle",
        #"NonClosure": "NonClosureNonClosure",
        "leptonWeight_EL_": "EG_",
        "leptonWeight_": "",
        "trigWeight_": "",
        "bTagWeight_": "",
        "jvtWeight_": "",
        "_systematics": "",
        #"Uncertainty": "",
        "EL_": "EG_",
        "_TOTAL_1NPCOR_PLUS_UNCOR": "",
        "extrapolation": "extrap",
        #"Efficiency": "eff",
        #"_TrigStatUncertainty": "_trigstat",
        #"_TrigSystUncertainty": "_trigsys",
        #"_Trigger": "_trig",
        #"_TrigStatUncertainty": "_trigeff",
        "FT_EFF_B": "btag_BT",
        "FT_EFF_C": "btag_CT",
        "FT_EFF_Light": "btag_LightT",
        "FT_EFF_extrap": "btag_Extra",
        "FT_EFF_extrap_from_charm": "btag_ExtraFromCharm",
        "JET_JvtEfficiency": "JvtEfficiency",
        "JET_fJvtEfficiency": "fJvtEfficiency",
        "pileup_wgt": "Pileup",
    }
    for k, v in replacements.items():
        label = label.replace(k, v)
    #label = label.lower()

    #if "OneSide" in method:
    #    down = None

    #print("2L2J label =",label, "nominal =",nominal)
    sys = Systematic(label, nominal, up, down, stype, method)
    # norm method for samples with normalisation factors
    # e.g. histoSys -> normHistoSys
    #      userHistoSys -> userNormHistoSys

    norm = method.replace("Histo", "NormHisto").replace("histo", "normHisto")
    sysnorm = Systematic(label, nominal, up, down, stype, method=norm)
    # add to samples, dependent on simulation mode
    onlyFull = {"EG_SCALE_ALL", "JET_JER_DataVsMC_MC16", "JET_PunchThrough_MC16"}
    onlyAF2 = {"JET_JER_DataVsMC_AFII", "JET_PunchThrough_AFII", "EG_SCALE_AF2"}
    if name not in onlyAF2:
        for T, samp in sysFull.items():
            if T not in normBkgs:
                Tsys = sys
            else:
                Tsys = sysnorm
            samp.addSystematic(Tsys)


def addSysTree(name, method="histoSys"):
    up = "_%s__1up" % name
    down = "_%s__1down" % name
    addSys(name, "_NoSys", up, down, "tree", method=method)


def addJetSys():
    """ Add JET and JER systematics. """
    systs = (
        "JET_EffectiveNP_Detector1",
        "JET_EffectiveNP_Detector2",
        "JET_EffectiveNP_Mixed1",
        "JET_EffectiveNP_Mixed2",
        "JET_EffectiveNP_Mixed3",
        "JET_EffectiveNP_Modelling1",
        "JET_EffectiveNP_Modelling2",
        "JET_EffectiveNP_Modelling3",
        "JET_EffectiveNP_Modelling4",
        "JET_EffectiveNP_Statistical1",
        "JET_EffectiveNP_Statistical2",
        "JET_EffectiveNP_Statistical3",
        "JET_EffectiveNP_Statistical4",
        "JET_EffectiveNP_Statistical5",
        "JET_EffectiveNP_Statistical6",
        "JET_EtaIntercalibration_Modelling",
        "JET_EtaIntercalibration_NonClosure_2018data",
        "JET_EtaIntercalibration_NonClosure_highE",
        "JET_EtaIntercalibration_NonClosure_negEta",
        "JET_EtaIntercalibration_NonClosure_posEta",
        "JET_EtaIntercalibration_TotalStat",
        "JET_Flavor_Composition",
        "JET_Flavor_Response",
        "JET_Pileup_OffsetMu",
        "JET_Pileup_OffsetNPV",
        "JET_Pileup_PtTerm",
        "JET_Pileup_RhoTopology",
        "JET_PunchThrough_AFII",
        "JET_PunchThrough_MC16",
    )
    # Notes: * - up == down, i - identical
    sysone = (
        "JET_BJES_Response",  # *
        # R10 -> large-R (R = 1.0) jets, which we do not use.
        #"JET_EffectiveNP_R10_1", # *1
        # "JET_EffectiveNP_R10_2", # *1
        # "JET_EffectiveNP_R10_3", # *1
        # "JET_EffectiveNP_R10_4", # *1
        # "JET_EffectiveNP_R10_5", # *1
        # "JET_EffectiveNP_R10_6restTerm", # *1
        #"JET_EtaIntercalibration_R10_TotalStat",  # *
        "JET_JER_DataVsMC_AFII", # *
        "JET_JER_DataVsMC_MC16", # *
        "JET_JER_EffectiveNP_1", # *
        "JET_JER_EffectiveNP_2", # *
        "JET_JER_EffectiveNP_3", # *
        "JET_JER_EffectiveNP_4", # *
        "JET_JER_EffectiveNP_5", # *
        # JET_JER_EffectiveNP_6 is not symmetric, but it looks small.
        # Putting it here to be with its friends.
        "JET_JER_EffectiveNP_6",
        "JET_JER_EffectiveNP_7restTerm",  # *
        "JET_SingleParticle_HighPt", # *
    )
    for sys in systs:
        addSysTree(sys)
    for sys in sysone:
        addSysTree(sys, "histoSysOneSideSym")



def addLepSys():
    """ Add EGamma, Muon and MET systematics. """
    # experimental systematics
    systs = (
        "EG_RESOLUTION_ALL",
        "EG_SCALE_AF2",
        "EG_SCALE_ALL",
        "MUON_ID",
        "MUON_MS",
        "MUON_SAGITTA_RESBIAS",
        "MUON_SCALE",
    )
    sysone = (
        "MUON_SAGITTA_RHO", # *
    )
    for sys in systs:
        addSysTree(sys)
    for sys in sysone:
        addSysTree(sys, "histoSysOneSideSym")

def addMetSys():
    ''' Add met systematics. '''
    addSys("MET_SoftTrk_ResoPara", "_NoSys", "_MET_SoftTrk_ResoPara",
           "_NoSys", "tree", "histoSysOneSide")
    addSys("MET_SoftTrk_ResoPerp", "_NoSys", "_MET_SoftTrk_ResoPerp",
           "_NoSys", "tree", "histoSysOneSide")
    addSys("MET_SoftTrk_Scale", "_NoSys", "_MET_SoftTrk_Scale__1up",
           "_MET_SoftTrk_Scale__1down", "tree", "histoSys")


def addPUsys():
    """ Add pileup reweighting systematics. """
    weights2L2J = ["genWeight", "eventWeight", "leptonWeight","jvtWeight", "pileupWeight", "bTagWeight", "AllWeight", "AllPR"]
    addSys("pileup_wgt", weights2L2J, replaceWeight(weights2L2J,"pileupWeight","pileupWeightUp"),replaceWeight(weights2L2J,"pileupWeight","pileupWeightDown"), "weight", "histoSys")


def addFNPsys():
    """ Add Matrix method fakes systematics. """
    weights2L2J = ["0.00000719604"]
    sys = Systematic("fake_wgt", weights2L2J, weights2L2J+["FNP_TOTAL_UP/FNP_WEIGHTS"], weights2L2J+["FNP_TOTAL_DOWN/FNP_WEIGHTS"],"weight", "histoSys")
    FNP2L2J.addSystematic(sys)



def addSysWeight(name, method="histoSys"):
    """ Utility to add standard weight sys. """
    denom = name.split("_")[0]
    weights2L2J = ["genWeight", "eventWeight", "leptonWeight","jvtWeight", "pileupWeight", "bTagWeight", "AllWeight", "AllPR"]
    if "leptonWeight" in name:
        addSys(name, weights2L2J, replaceWeight(weights2L2J,"leptonWeight","%s__1up"%name), replaceWeight(weights2L2J,"leptonWeight","%s__1down"%name), "weight", method=method)
    elif "trigWeight" in name:
        denom = "globalDiLepTrigSF"
        addSys(name, weights2L2J, weights2L2J + ["%s__1up/%s" % (name, denom)], weights2L2J + ["%s__1down/%s" % (name, denom)], "weight", method=method)
    elif "bTagWeight" in name:
        addSys(name, weights2L2J, replaceWeight(weights2L2J,"bTagWeight","%s__1up"%name), replaceWeight(weights2L2J,"bTagWeight","%s__1down"%name), "weight", method=method)
    elif "jvtWeight" in name:
        addSys(name, weights2L2J, replaceWeight(weights2L2J,"jvtWeight","%s__1up"%name), replaceWeight(weights2L2J,"jvtWeight","%s__1down"%name), "weight", method=method)


def addWeightSys():
    """ Add weight-based systematics. """
    weightSysts = (
        # TYPE: LEP
        "leptonWeight_EL_CHARGEID_STAT",
        "leptonWeight_EL_CHARGEID_SYStotal",
        "leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",
        "leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
        "leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
        "leptonWeight_MUON_EFF_ISO_STAT",
        "leptonWeight_MUON_EFF_ISO_SYS",
        "leptonWeight_MUON_EFF_RECO_STAT",
        "leptonWeight_MUON_EFF_RECO_SYS",
        "leptonWeight_MUON_EFF_TTVA_STAT",
        "leptonWeight_MUON_EFF_TTVA_SYS",
        # TYPE: TRIG
        "trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR",
        "trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
        "trigWeight_MUON_EFF_TrigStatUncertainty",
        "trigWeight_MUON_EFF_TrigSystUncertainty",
        # TYPE: BTAG
        "bTagWeight_FT_EFF_B_systematics",
        "bTagWeight_FT_EFF_C_systematics",
        "bTagWeight_FT_EFF_Light_systematics",
        "bTagWeight_FT_EFF_extrapolation",
        "bTagWeight_FT_EFF_extrapolation_from_charm",
        # TYPE: JVT
        "jvtWeight_JET_JvtEfficiency",
        "jvtWeight_JET_fJvtEfficiency",
        # These systematics have no effect on our analysis.
        # "leptonWeight_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR",
        # "leptonWeight_MUON_EFF_RECO_STAT_LOWPT",
        # "leptonWeight_MUON_EFF_RECO_SYS_LOWPT",
    )
    for sys in weightSysts:
        addSysWeight(sys)

if debug == False:
    addJetSys()
    addLepSys()
    addMetSys()
    addWeightSys()
    addPUsys()
    addFNPsys()


# ********************************************************************* #
#                              Background-only config
# ********************************************************************* #

bkgOnly = configMgr.addFitConfig("bkgonly")

# creating now list of samples
samplelist = []
if "ttbar" in mysamples: samplelist.append(TtbarSample)
if "singletop" in mysamples: samplelist.append(SingleTopSample)
if "wjets_Sh_2211" in mysamples: samplelist.append(WSample)
if "diboson" in mysamples: 
    samplelist.append(Diboson1LSample)
    samplelist.append(Diboson2LSample)
    samplelist.append(Diboson0LSample)
if "multiboson" in mysamples: samplelist.append(TribosonSample)
if "vh" in mysamples: samplelist.append(VHSample)
if "zjets_Sh_2211" in mysamples: samplelist.append(ZSample)
if "tth" in mysamples: samplelist.append(TtbarHSample)
if "ttv" in mysamples: samplelist.append(TtbarVSample)

if "data" in mysamples: samplelist.append(DataSample)

if doOnlySignal: samplelist = [SignalSample, DataSample]

#bkgOnly.addSamples(samplelist)

if useStat:
    bkgOnly.statErrThreshold = 0.001
else:
    bkgOnly.statErrThreshold = None

# Add measurement
meas = bkgOnly.addMeasurement("BasicMeasurement", lumi = 1.0, lumiErr = 0.017)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi", "const", 1.0)

# Classification of channels
bTagChans = {}
nonBTagChans = {}
ZTagChans = {}
WTagChans = {}
elChans = {}
muChans = {}
elmuChans = {}

for region in CRregions:
    bTagChans[region] = []
    nonBTagChans[region] = []
    ZTagChans[region] = []
    WTagChans[region] = []
    elChans[region] = []
    muChans[region] = []
    elmuChans[region] = []

######################################################
# Add channels to Bkg-only configuration             #
######################################################

if "resolved" in CRregions:
    tmp = appendTo(bkgOnly.addChannel("cuts", ["WDB1LCRresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
    bkgOnly.addBkgConstrainChannels(tmp)

    tmp = appendTo(bkgOnly.addChannel("cuts", ["TCRresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
    bkgOnly.addBkgConstrainChannels(tmp)

    DB2LCREM = bkgOnly.addChannel("cuts", ["DB2LCREM"], 1, 0.5, 1.5)
    appendTo(DB2LCREM, [bTagChans["general"], elmuChans["general"]])
    bkgOnly.addBkgConstrainChannels(DB2LCREM)
    for sample in samplelist:
        DB2LCREM.addSample(sample)


if "boosted" in CRregions:

    
    # 1 Lepton
    WDB1LCRboostedEM = bkgOnly.addChannel("meffInc30", ["WDB1LCRboostedEM"], 2, 600., 1100.)
    appendTo(WDB1LCRboostedEM, [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
    bkgOnly.addBkgConstrainChannels(WDB1LCRboostedEM)
    for sample in samplelist:
        WDB1LCRboostedEM.addSample(sample)

    TCRboostedEM = bkgOnly.addChannel("meffInc30", ["TCRboostedEM"], 2, 600., 1100.)
    appendTo(TCRboostedEM, [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
    bkgOnly.addBkgConstrainChannels(TCRboostedEM)
    for sample in samplelist:
        TCRboostedEM.addSample(sample)

    DB2LCREM = bkgOnly.addChannel("cuts", ["DB2LCREM"], 1, 0.5, 1.5)
    appendTo(DB2LCREM, [bTagChans["general"], elmuChans["general"]])
    bkgOnly.addBkgConstrainChannels(DB2LCREM)
    for sample in samplelist:
        DB2LCREM.addSample(sample)
    
    
    # 2 Leptons
    CR_VZ2L2J = bkgOnly.addChannel("cuts",['CRVZ'],1,0.5,1.5)

    for sample in samplelist2L2J:
        CR_VZ2L2J.addSample(sample)
    CR_VZ2L2J.removeWeight('trigWeight_singleLepTrig')
    CR_VZ2L2J.removeWeight('polWeight')

    
    CR_tt = bkgOnly.addChannel("cuts",['CRtt'],1,0.5,1.5)
    for sample in samplelist2L2J:
        CR_tt.addSample(sample)
    CR_tt.removeWeight('trigWeight_singleLepTrig')
    CR_tt.removeWeight('polWeight')

    # SUSY-recommended extrapolation on b-jets
    CR_tt.getSample("Zjets").addSystematic(Systematic("Zjets_bextrap", 1.0, 1.3, 0.7,"user", "userNormHistoSys"))
    CR_tt.getSample("Zyjets").addSystematic(Systematic("Zjets_bextrap", 1.0, 1.3, 0.7,"user", "userNormHistoSys"))


    CR_Z = bkgOnly.addChannel("cuts",['CRZ'],1,0.5,1.5)
    for sample in samplelist2L2J:
        CR_Z.addSample(sample)
    CR_Z.removeWeight('trigWeight_singleLepTrig')
    CR_Z.removeWeight('polWeight')



    CR_DY = bkgOnly.addChannel("cuts",['CRDY'],1,0.5,1.5)
    for sample in samplelist2L2J:
        CR_DY.addSample(sample)
    CR_DY.removeWeight('trigWeight_singleLepTrig')
    CR_DY.removeWeight('polWeight')
    




    # 2L2J Theory Syst
    for sys in EWKtheory.getscales("Zjets", "CRZZ"):
        CR_VZ2L2J.getSample("Zjets").addSystematic(sys)
    for sys in EWKtheory.getscales("Zjets", "CRZZ"):
        CR_VZ2L2J.getSample("Zyjets").addSystematic(sys)
    for sys in EWKtheory.getscales("diboson", "CRZZ"):
        CR_VZ2L2J.getSample("diboson2l").addSystematic(sys)
    for sys in EWKtheory.getscales("ttbar", "CRZZ"):
        CR_VZ2L2J.getSample("ttbar").addSystematic(sys)
    for sys in EWKtheory.getscales("topOther", "CRZZ"):
        CR_VZ2L2J.getSample("topOther").addSystematic(sys)
    for sys in EWKtheory.getscales("singleTop", "CRZZ"):
        CR_VZ2L2J.getSample("singleTop").addSystematic(sys)
    for sys in EWKtheory.get_ttbar_vars("CRZZ"):
        CR_VZ2L2J.getSample("ttbar").addSystematic(sys)
    for sys in EWKtheory.get_ttv_vars("CRZZ"):
        CR_VZ2L2J.getSample("topOther").addSystematic(sys)
    # extra theory systematics derived from truth
    CR_VZ2L2J.getSample("Zjets").addSystematic(EWKtheory.get_zjets_var("Zjets","CRZZ"))
    CR_VZ2L2J.getSample("Zyjets").addSystematic(EWKtheory.get_zjets_var("Zjets","CRZZ"))
    for sys in EWKtheory.get_diboson_extra_vars("diboson","CRZZ"):
        CR_VZ2L2J.getSample("diboson2l").addSystematic(sys)


    for sys in EWKtheory.getscales("Zjets", "CRtt"):
        CR_tt.getSample("Zjets").addSystematic(sys)
    for sys in EWKtheory.getscales("Zjets", "CRtt"):
        CR_tt.getSample("Zyjets").addSystematic(sys)
    for sys in EWKtheory.getscales("diboson", "CRtt"):
        CR_tt.getSample("diboson2l").addSystematic(sys)
    for sys in EWKtheory.getscales("ttbar", "CRtt"):
        CR_tt.getSample("ttbar").addSystematic(sys)
    for sys in EWKtheory.getscales("topOther", "CRtt"):
        CR_tt.getSample("topOther").addSystematic(sys)
    for sys in EWKtheory.getscales("singleTop", "CRtt"):
        CR_tt.getSample("singleTop").addSystematic(sys)
    for sys in EWKtheory.get_ttbar_vars("CRtt"):
        CR_tt.getSample("ttbar").addSystematic(sys)
    for sys in EWKtheory.get_ttv_vars("CRtt"):
        CR_tt.getSample("topOther").addSystematic(sys)
    # extra theory systematics derived from truth
    CR_tt.getSample("Zjets").addSystematic(EWKtheory.get_zjets_var("Zjets","CRtt"))
    CR_tt.getSample("Zyjets").addSystematic(EWKtheory.get_zjets_var("Zjets","CRtt"))
    for sys in EWKtheory.get_diboson_extra_vars("diboson","CRtt"):
        CR_tt.getSample("diboson2l").addSystematic(sys)


    for sys in EWKtheory.getscales("Zjets", "CRZ"):
        CR_Z.getSample("Zjets").addSystematic(sys)
    for sys in EWKtheory.getscales("Zjets", "CRZ"):
        CR_Z.getSample("Zyjets").addSystematic(sys)
    for sys in EWKtheory.getscales("diboson", "CRZ"):
        CR_Z.getSample("diboson2l").addSystematic(sys)
    for sys in EWKtheory.getscales("ttbar", "CRZ"):
        CR_Z.getSample("ttbar").addSystematic(sys)
    for sys in EWKtheory.getscales("topOther", "CRZ"):
        CR_Z.getSample("topOther").addSystematic(sys)
    for sys in EWKtheory.getscales("singleTop", "CRZ"):
        CR_Z.getSample("singleTop").addSystematic(sys)
    for sys in EWKtheory.get_ttbar_vars("CRZ"):
        CR_Z.getSample("ttbar").addSystematic(sys)
    for sys in EWKtheory.get_ttv_vars("CRZ"):
        CR_Z.getSample("topOther").addSystematic(sys)
    # extra theory systematics derived from truth
    CR_Z.getSample("Zjets").addSystematic(EWKtheory.get_zjets_var("Zjets","CRZ"))
    CR_Z.getSample("Zyjets").addSystematic(EWKtheory.get_zjets_var("Zjets","CRZ"))
    for sys in EWKtheory.get_diboson_extra_vars("diboson","CRZ"):
        CR_Z.getSample("diboson2l").addSystematic(sys)


    for sys in EWKtheory.getscales("Zjets", "CRDY"):
        CR_DY.getSample("Zjets").addSystematic(sys)
    for sys in EWKtheory.getscales("Zjets", "CRDY"):
        CR_DY.getSample("Zyjets").addSystematic(sys)
    for sys in EWKtheory.getscales("diboson", "CRDY"):
        CR_DY.getSample("diboson2l").addSystematic(sys)
    for sys in EWKtheory.getscales("ttbar", "CRDY"):
        CR_DY.getSample("ttbar").addSystematic(sys)
    for sys in EWKtheory.getscales("topOther", "CRDY"):
        CR_DY.getSample("topOther").addSystematic(sys)
    for sys in EWKtheory.getscales("singleTop", "CRDY"):
        CR_DY.getSample("singleTop").addSystematic(sys)
    for sys in EWKtheory.get_ttbar_vars("CRDY"):
        CR_DY.getSample("ttbar").addSystematic(sys)
    for sys in EWKtheory.get_ttv_vars("CRDY"):
        CR_DY.getSample("topOther").addSystematic(sys)
    # extra theory systematics derived from truth
    CR_DY.getSample("Zjets").addSystematic(EWKtheory.get_zjets_var("Zjets","CRDY"))
    CR_DY.getSample("Zyjets").addSystematic(EWKtheory.get_zjets_var("Zjets","CRDY"))
    for sys in EWKtheory.get_diboson_extra_vars("diboson","CRDY"):
        CR_DY.getSample("diboson2l").addSystematic(sys)

    bkgOnly.addBkgConstrainChannels([CR_VZ2L2J,CR_tt,CR_Z,CR_DY])


    SRLMboostedWZEM = bkgOnly.addChannel("meffInc30", ["SRLMboostedWZEM"], 2, 600., 1100.)
    appendTo(SRLMboostedWZEM, [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
    for sample in samplelist:
        SRLMboostedWZEM.addSample(sample)

    SRMMboostedWZEM = bkgOnly.addChannel("meffInc30", ["SRMMboostedWZEM"], 2, 600., 1100.)
    appendTo(SRMMboostedWZEM, [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
    for sample in samplelist:
        SRMMboostedWZEM.addSample(sample)

    SRHMboostedWZEM = bkgOnly.addChannel("meffInc30", ["SRHMboostedWZEM"], 2, 600., 1100.)
    appendTo(SRHMboostedWZEM, [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
    for sample in samplelist:
        SRHMboostedWZEM.addSample(sample)

    bkgOnly.addSignalChannels([SRLMboostedWZEM,SRMMboostedWZEM,SRHMboostedWZEM])

for v in bkgOnly.channels:
    if not "cuts" in v.name: v.useOverflowBin = True


# ********************************************************************* #
#                + validation regions (including signal regions)
# ********************************************************************* #

#you can use this statement here to add further groups of validation regions that you might find useful
ValidRegList["OneLep"] = False
for region in CRregions:
    ValidRegList["OneLep"] += ValidRegList[region]

validation = None
# run validation regions for either table input or plots in VRs
if doTableInputs or ValidRegList["OneLep"] or VRplots:
    validation = configMgr.addFitConfigClone(bkgOnly, "Validation")
    for c in validation.channels:
        for region in CRregions:
            appendIfMatchName(c, bTagChans[region])
            appendIfMatchName(c, nonBTagChans[region])
            appendIfMatchName(c, ZTagChans[region])
            appendIfMatchName(c, WTagChans[region])
            appendIfMatchName(c, elChans[region])
            appendIfMatchName(c, muChans[region])
            appendIfMatchName(c, elmuChans[region])

    if doTableInputs:
        if "resolved" in CRregions:
            appendTo(validation.addValidationChannel("cuts", ["WDB1LVRresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
            appendTo(validation.addValidationChannel("cuts", ["TVRresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
            appendTo(validation.addValidationChannel("cuts", ["SRLMresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])
            appendTo(validation.addValidationChannel("cuts", ["SRHMresolvedEM"], 1, 0.5, 1.5), [bTagChans["resolved"], elmuChans["resolved"]])

        if "boosted" in CRregions:
            appendTo(validation.addValidationChannel("meffInc30", ["WDB1LVR1boostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            appendTo(validation.addValidationChannel("meffInc30", ["WDB1LVR2boostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            appendTo(validation.addValidationChannel("meffInc30", ["TVR1boostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            appendTo(validation.addValidationChannel("meffInc30", ["TVR2boostedEM"], 2, 600., 1100.), [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            if not doBackupCacheFile:
                if myAna == "C1N2_WZ":
                    appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("cuts", ["SRLMboostedWZdiscEM"],1, 0.5, 1.5), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("cuts", ["SRMMboostedWZdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("cuts", ["SRHMboostedWZdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                if myAna == "C1C1_WW":
                    appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("cuts", ["SRLMboostedWWdiscEM"],1, 0.5, 1.5), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("cuts", ["SRMMboostedWWdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                    appendTo(validation.addValidationChannel("cuts", ["SRHMboostedWWdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            else:
                appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWZEM"], 2, 600., 1100.), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRLMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRMMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("meffInc30", ["SRHMboostedWWEM"], 2, 600., 1100.), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
		
               	appendTo(validation.addValidationChannel("cuts", ["SRLMboostedWZdiscEM"],1, 0.5, 1.5), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("cuts", ["SRMMboostedWZdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("cuts", ["SRHMboostedWZdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], ZTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("cuts", ["SRLMboostedWWdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("cuts", ["SRMMboostedWWdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
                appendTo(validation.addValidationChannel("cuts", ["SRHMboostedWWdiscEM"], 1, 0.5, 1.5), [nonBTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])

        if doBlindSRinBGfit:
            if "resolved" in CRregions:
                validation.getChannel("cuts", ["SRLMresolvedEM"]).blind = True
                validation.getChannel("cuts", ["SRHMresolvedEM"]).blind = True
            if "boosted" in CRregions:
                if not doBackupCacheFile:
                    if myAna == "C1N2_WZ":
                        validation.getChannel("meffInc30", ["SRLMboostedWZEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRMMboostedWZEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRHMboostedWZEM"]).blind = True
                        validation.getChannel("cuts", ["SRLMboostedWZdiscEM"]).blind = True
                        validation.getChannel("cuts", ["SRMMboostedWZdiscEM"]).blind = True
                        validation.getChannel("cuts", ["SRHMboostedWZdiscEM"]).blind = True
                    if myAna == "C1C1_WW":
                        validation.getChannel("meffInc30", ["SRLMboostedWWEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRMMboostedWWEM"]).blind = True
                        validation.getChannel("meffInc30", ["SRHMboostedWWEM"]).blind = True
                        validation.getChannel("cuts", ["SRLMboostedWWdiscEM"]).blind = True
                        validation.getChannel("cuts", ["SRMMboostedWWdiscEM"]).blind = True
                        validation.getChannel("cuts", ["SRHMboostedWWdiscEM"]).blind = True
                else:
                    validation.getChannel("meffInc30", ["SRLMboostedWZEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRMMboostedWZEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRHMboostedWZEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRLMboostedWWEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRMMboostedWWEM"]).blind = True
                    validation.getChannel("meffInc30", ["SRHMboostedWWEM"]).blind = True
                    validation.getChannel("cuts", ["SRLMboostedWZdiscEM"]).blind = True
                    validation.getChannel("cuts", ["SRMMboostedWZdiscEM"]).blind = True
                    validation.getChannel("cuts", ["SRHMboostedWZdiscEM"]).blind = True
                    validation.getChannel("cuts", ["SRLMboostedWWdiscEM"]).blind = True
                    validation.getChannel("cuts", ["SRMMboostedWWdiscEM"]).blind = True
                    validation.getChannel("cuts", ["SRHMboostedWWdiscEM"]).blind = True


        if "general" in CRregions:
            DB2LVREM = validation.addChannel("cuts", ["DB2LVREM"], 1, 0.5, 1.5)
            appendTo(DB2LVREM, [bTagChans["general"],  elmuChans["general"]])
            validation.addValidationChannels(DB2LVREM)
            for sample in samplelist:
                DB2LVREM.addSample(sample)

        # All validation regions should use over-flow bins
        for v in validation.channels:
            if not "cuts" in v.name: v.useOverflowBin = True
	    
    if VRplots:
        '''
        if "general" in CRregions:
            DB2LVREM = validation.addChannel("cuts", ["DB2LVREM"], 1, 0.5, 1.5)
            appendTo(DB2LVREM, [bTagChans["general"],  elmuChans["general"]])
            validation.addValidationChannels(DB2LVREM)
            for sample in samplelist:
                DB2LVREM.addSample(sample)

            WDB1LVR1boostedEM = validation.addChannel("meffInc30", ["WDB1LVR1boostedEM"], 2, 600., 1100.)
            appendTo(WDB1LVR1boostedEM, [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            validation.addValidationChannels(WDB1LVR1boostedEM)
            for sample in samplelist:
                WDB1LVR1boostedEM.addSample(sample)

            WDB1LVR2boostedEM = validation.addChannel("meffInc30", ["WDB1LVR2boostedEM"], 2, 600., 1100.)
            appendTo(WDB1LVR2boostedEM, [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            validation.addValidationChannels(WDB1LVR2boostedEM)
            for sample in samplelist:
                WDB1LVR2boostedEM.addSample(sample)

            TVR1boostedEM = validation.addChannel("meffInc30", ["TVR1boostedEM"], 2, 600., 1100.)
            appendTo(TVR1boostedEM, [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            validation.addValidationChannels(TVR1boostedEM)
            for sample in samplelist:
                TVR1boostedEM.addSample(sample)

            TVR2boostedEM = validation.addChannel("meffInc30", ["TVR2boostedEM"], 2, 600., 1100.)
            appendTo(TVR2boostedEM, [bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
            validation.addValidationChannels(TVR2boostedEM)
            for sample in samplelist:
                TVR2boostedEM.addSample(sample)

        DB2LCRMETSigEM = validation.addChannel("met_Signif",["DB2LCRMETSigEM"],8,12.,20.)
        appendTo(DB2LCRMETSigEM,[bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
        validation.addValidationChannels(DB2LCRMETSigEM)
        for sample in samplelist:
            DB2LCRMETSigEM.addSample(sample)

        DB2LVRMETSigEM = validation.addChannel("met_Signif",["DB2LVRMETSigEM"],10,10.,20.)
        appendTo(DB2LVRMETSigEM,[bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
        validation.addValidationChannels(DB2LVRMETSigEM)
        for sample in samplelist:
            DB2LVRMETSigEM.addSample(sample)

        DB2LCRmTEM = validation.addChannel("mt",["DB2LCRmTEM"],5,50.,200.)
        appendTo(DB2LCRmTEM,[bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
        validation.addValidationChannels(DB2LCRmTEM)
        for sample in samplelist:
            DB2LCRmTEM.addSample(sample)

        DB2LVRmTEM = validation.addChannel("mt",["DB2LVRmTEM"],5,200.,350.)
        appendTo(DB2LVRmTEM,[bTagChans["boosted"], WTagChans["boosted"], elmuChans["boosted"]])
        validation.addValidationChannels(DB2LVRmTEM)
        for sample in samplelist:
            DB2LVRmTEM.addSample(sample)
        '''
	# All validation regions should use over-flow bins!
        for v in validation.channels : 
            if not 'cuts' in v.name: v.useOverflowBin=True
	

# ********************************************************************* #
#                              Exclusion fit
# ********************************************************************* #

if myFitType == FitType.Exclusion:

    if myAna == "C1N2_WZ": signal_uncertainties = SignalTheoryUncertainties ("./systWZ.root")
    if myAna == "C1C1_WW": signal_uncertainties = SignalTheoryUncertainties ("./systWW.root")

    SR_channels = {}
    SRs = ["SRLMboostedWZEM", "SRMMboostedWZEM", "SRHMboostedWZEM"]

    for sig in sigSamples:
        SR_channels[sig] = []
        sig2 = sig.replace("p5", "").replace("p0", "")
        myTopLvl = configMgr.addFitConfigClone(bkgOnly, "Sig_%s"%sig2)
        for c in myTopLvl.channels:
            for region in CRregions:
                appendIfMatchName(c, bTagChans[region])
                appendIfMatchName(c, nonBTagChans[region])
                appendIfMatchName(c, ZTagChans[region])
                appendIfMatchName(c, WTagChans[region])
                appendIfMatchName(c, elChans[region])
                appendIfMatchName(c, muChans[region])
                appendIfMatchName(c, elmuChans[region])
            if not "cuts" in c.name: c.useOverflowBin = True

        sigSample = Sample(sig2, kPink)
        sigSample.setPrefixTreeName(sig)
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG", 1., 0., 5.)

        # signal-specific uncertainties
        sigSample.setStatConfig(useStat)
        if not debug: sigSample.addSystematic(xsecSig)

        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)

        for sr in SRs:
            if myAna == "C1N2_WZ":
                ERRregion = "WZ_SR_boost"
            if myAna == "C1C1_WW":
                ERRregion = "WW_SR_boost"

            ############## Signal theory uncertainties ###############
            if not doHistoBuilding and ("C1N2_WZ" in sig or "C1C1_WW" in sig) and not debug:
                print (sig)
                errsigList = signal_uncertainties.getUncertainties(ERRregion, sig)
                errsig = []
                errsig += errsigList
                print ("sig=", sig, " region=", ERRregion, " errsig_qcup=", errsig[0], " errsig_qcdw=", errsig[1], " errsig_scup=", errsig[2], " errsig_scdw=", errsig[3], " errsig_raup=", errsig[4], " errsig_radw=", errsig[5])
                if "resolved" in CRregions:
                    theoSigQc = Systematic("Qc", configMgr.weights, [(1.+errsig[0])], [(1.+errsig[1])], "user", "userHistoSys")
                    theoSigSc = Systematic("Sc", configMgr.weights, [(1.+errsig[2])], [(1.+errsig[3])], "user", "userHistoSys")
                    theoSigRad = Systematic("Rad", configMgr.weights, [(1.+errsig[4])], [(1.+errsig[5])], "user", "userHistoSys")
                if "boosted" in CRregions:
                    theoSigQc = Systematic("Qc", configMgr.weights, [(1.+errsig[0]), (1.+errsig[0])], [(1.+errsig[1]), (1.+errsig[1])], "user", "userHistoSys")
                    theoSigSc = Systematic("Sc", configMgr.weights, [(1.+errsig[2]), (1.+errsig[2])], [(1.+errsig[3]), (1.+errsig[3])], "user", "userHistoSys")
                    theoSigRad = Systematic("Rad", configMgr.weights, [(1.+errsig[4]), (1.+errsig[4])], [(1.+errsig[5]), (1.+errsig[5])], "user", "userHistoSys")
                myTopLvl.getChannel("meffInc30", [sr]).getSample(sig2).addSystematic(theoSigQc)
                myTopLvl.getChannel("meffInc30", [sr]).getSample(sig2).addSystematic(theoSigSc)
                myTopLvl.getChannel("meffInc30", [sr]).getSample(sig2).addSystematic(theoSigRad)
            ##########################################################

                SR_channels[sig].append(myTopLvl.getChannel("meffInc30", [sr]))

# ************************************************************************************* #
#                     Finalization of fitConfigs (add systematics and input samples)
# ************************************************************************************* #
normRegions = []

if "resolved" in CRregions:
    normRegions += [("WDB1LCRresolvedEM", "cuts"), ("TCRresolvedEM", "cuts")]
if "boosted" in CRregions:
    normRegions += [("WDB1LCRboostedEM", "meffInc30"), ("TCRboostedEM", "meffInc30")]
if "general" in CRregions:
    normRegions += [("DB2LCREM", "cuts")]




AllChannels = {}
AllChannels_all = []
elChans_all = []
muChans_all = []
elmuChans_all = []

for region in CRregions:
    AllChannels[region] = bTagChans[region] + nonBTagChans[region]
    AllChannels_all += AllChannels[region]
    elChans_all += elChans[region]
    muChans_all += muChans[region]
    elmuChans_all += elmuChans[region]

# Generator Systematics for each sample,channel
log.info("** Generator Systematics **")
for tgt,syst in generatorSyst:
    tgtsample = tgt[0]  
    tgtchan = tgt[1]
    #print tgtsample,tgtchan

    for chan in AllChannels_all:
        #if tgtchan=="All" or tgtchan==chan.name:
	    #print "here" ,tgtsample, tgtchan, chan.name
        if (tgtchan=="All" or tgtchan in chan.name) and not doOnlySignal and not debug:
	        #print "after", tgtchan	  
            chan.getSample(tgtsample).addSystematic(syst)
            #log.info("Add Generator Systematics (%s) to (%s)" %(syst.name, chan.name))

if not doOnlySignal:
    for region in CRregions:
        SetupChannels(elChans[region], bkgFiles_e, basicChanSyst[region])
        SetupChannels(muChans[region], bkgFiles_m, basicChanSyst[region])
        SetupChannels(elmuChans[region], bkgFiles_em, basicChanSyst[region])

# Final semi-hacks for signal samples in exclusion fits
if myFitType == FitType.Exclusion and not doHistoBuilding:
    for sig in sigSamples:
        sig2 = sig.replace("p5", "").replace("p0", "")
        myTopLvl = configMgr.getFitConfig("Sig_%s"%sig2)
        for chan in myTopLvl.channels:
            theSample = chan.getSample(sig2)
            # replacement of JES_PunchThrough and JER_DataVsMC systematic for AF2 signal samples (not for FullSim signal samples)
            if not debug and sig in FastSimSig:
                if chan.hasSample(theSample):
                    print ("This is an AFII signal sample -> removing systematics JER_DataVsMC_MC16 and JES_PunchThrough_MC16")
                    theSample.removeSystematic("JER_DataVsMC_MC16")
                    theSample.removeSystematic("JES_PunchThrough_MC16")
                    print ("and adding systematics JER_DataVsMC_AFII and JES_PunchThrough_AFII")
                    theSample.addSystematic(Systematic("JER_DataVsMC_AFII", "_NoSys", "_JET_JER_DataVsMC_AFII__1up", "_JET_JER_DataVsMC_AFII__1down", "tree", "overallNormHistoSys"))
                    theSample.addSystematic(Systematic("JES_PunchThrough_AFII", "_NoSys", "_JET_PunchThrough_AFII__1up", "_JET_PunchThrough_AFII__1down", "tree", "overallNormHistoSys"))
            #if debug:
            #    theSample.removeSystematic("dummy")

            theSigFiles = []
            if chan in elChans_all: theSigFiles = sigFiles_e[sig]
            elif chan in muChans_all: theSigFiles = sigFiles_m[sig]
            elif chan in elmuChans_all: theSigFiles = sigFiles_em[sig]
            #elif chan in [CR_VZ2L2J,CR_tt,CR_Z,CR_DY]: print("Skipping 2L2J regions here")
            #else: raise ValueError("Unexpected channel name %s"%(chan.name))

            if len(theSigFiles)>0: theSample.addInputs(theSigFiles)
            else: 
                print ("ERROR no signal file for %s in channel %s. Remove Sample."%(theSample.name, chan.name))
                chan.removeSample(theSample)

# Add b-tag, boson-tag systematic
for region in CRregions:
    for chan in bTagChans[region]:
        if "BTag" in SystList and not doOnlySignal:
            chan.addSystematic(bTagSyst[region])
            chan.addSystematic(cTagSyst[region])    
            chan.addSystematic(mTagSyst[region])
            chan.addSystematic(eTagSyst[region])
            chan.addSystematic(eTagFromCSyst[region])

    """ for chan in ZTagChans[region]:
        if "BSTag" in SystList and not doOnlySignal:
            # Add boson tagging sf syst

    for chan in WTagChans[region]:
        if "BSTag" in SystList and not doOnlySignal:
            # Add boson tagging sf syst """
    
    for chan in (bTagChans[region] + nonBTagChans[region]):
        if not doOnlySignal:
            if "ttbar" in mysamples:
                if ("CRVZ" in chan.name):
                    chan.getSample("ttbar").removeSystematic(normRegions)
                chan.getSample("ttbar").setNormRegions(normRegions)
            if OneNFTopST:
                        if "singletop" in mysamples:
                                 chan.getSample("singletop").setNormRegions(normRegions)
                                 if not debug:chan.getSample("singletop").addSystematic(Systematic("singletop_XS", configMgr.weights, addWeight(weights, "(((DatasetNumber==410644 || DatasetNumber==410658)*1.04) + ((DatasetNumber!=410644 && DatasetNumber!=410658)*1.05))"), addWeight(weights, "(((DatasetNumber==410644 || DatasetNumber==410658)*0.96) + ((DatasetNumber!=410644 && DatasetNumber!=410658)*0.95))"), "weight", "overallSys"))
            else:
                         if "singletop" in mysamples and not debug:
                                chan.getSample("singletop").addSystematic(Systematic("singletop_XS", configMgr.weights, addWeight(weights, "(((DatasetNumber==410644 || DatasetNumber==410658)*1.04) + ((DatasetNumber!=410644 && DatasetNumber!=410658)*1.05))"), addWeight(weights, "(((DatasetNumber==410644 || DatasetNumber==410658)*0.96) + ((DatasetNumber!=410644 && DatasetNumber!=410658)*0.95))"), "weight", "overallSys"))
            
            if "wjets_Sh_2211" in mysamples:
                chan.getSample("wjets_Sh_2211").setNormRegions(normRegions)
                ###############sh2211 have very large eventweights, so not remove large eventweights###################
               # chan.getSample("wjets").removeWeight("eventWeight")
               # chan.getSample("wjets").addWeight("((fabs(eventWeight)<100.)*eventWeight + (eventWeight>100.) - (eventWeight<-100.))")
                if "WDB1LCR" in chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.886+ (meffInc30>850.)*0.835)")
                if "WDB1LVR1" in chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.907+ (meffInc30>850.)*0.871)")
                if "WDB1LVR2" in chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.852+ (meffInc30>850.)*0.802)")
                if "TCR" in chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.874+ (meffInc30>850.)*0.830)")
                if "TVR1" in chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.881+ (meffInc30>850.)*0.853)")
                if "TVR2" in chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.856+ (meffInc30>850.)*0.751)")
                if "DB2LCR" in chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("0.917")
                if "DB2LVR" in chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("0.931")
                if "meffInc30_SRLMboostedWWEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.890+ (meffInc30>850.)*0.887)")
                if "meffInc30_SRMMboostedWWEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.912+ (meffInc30>850.)*0.774)")
                if "meffInc30_SRHMboostedWWEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.926+ (meffInc30>850.)*0.898)")
                if "cuts_SRLMboostedWWdiscEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("0.913")
                if "cuts_SRMMboostedWWdiscEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("0.890")
                if "cuts_SRHMboostedWWdiscEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("0.939")
                if "meffInc30_SRLMboostedWZEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.915+ (meffInc30>850.)*0.847)")
                if "meffInc30_SRMMboostedWZEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.932+ (meffInc30>850.)*0.819)")
                if "meffInc30_SRHMboostedWZEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("( (meffInc30<850.)*0.903+ (meffInc30>850.)*0.926)")
                if "cuts_SRLMboostedWZdiscEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("0.915")
                if "cuts_SRMMboostedWZdiscEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("0.819")
                if "cuts_SRHMboostedWZdiscEM" == chan.name: 
                        chan.getSample("wjets_Sh_2211").addWeight("0.926")

            if "diboson" in mysamples:
                chan.getSample("diboson1l").setNormRegions(normRegions)
                chan.getSample("diboson2l").setNormRegions(normRegions)
                if not debug: chan.getSample("diboson0l").addSystematic(Systematic("diboson_XS", configMgr.weights, 1.06, 0.94, "user", "userOverallSys"))

            if "multiboson" in mysamples and not debug:
                chan.getSample("multiboson").addSystematic(Systematic("multiboson_XS", configMgr.weights, 1.2, 0.8, "user", "userOverallSys"))

            if "tth" in mysamples and not debug:
                chan.getSample("tth").addSystematic(Systematic("tth_XS", configMgr.weights, 1.1, 0.9, "user", "userOverallSys"))

            if "ttv" in mysamples and not debug:
                chan.getSample("ttv").addSystematic(Systematic("ttv_XS", configMgr.weights, addWeight(weights, "(((DatasetNumber==410155)*1.13) + ((DatasetNumber!=410155)*1.12))"), addWeight(weights, "(((DatasetNumber==410155)*0.87) + ((DatasetNumber!=410155)*0.88))"), "weight", "overallSys"))

            if "zjets_Sh_2211" in mysamples and not debug:
                chan.getSample("zjets_Sh_2211").addSystematic(Systematic("zjets_Sh_2211_XS", configMgr.weights, 1.05, 0.95, "user", "userOverallSys"))

            if "zjets_Sh_2211" in mysamples:
                if "WDB1LCR" in chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*0.95+ (meffInc30>850.)*0.92)")
                if "WDB1LVR1" in chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*0.99+ (meffInc30>850.)*0.90)")
                if "WDB1LVR2" in chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*0.90+ (meffInc30>850.)*0.88)")
                if "TCR" in chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*0.96+ (meffInc30>850.)*1.00)")
                if "TVR1" in chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*1.00+ (meffInc30>850.)*0.92)")
                if "TVR2" in chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*0.86+ (meffInc30>850.)*0.90)")
                if "DB2LCR" in chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("1.00")
                if "DB2LVR" in chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("0.99")
                if "meffInc30_SRLMboostedWWEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*0.88+ (meffInc30>850.)*1.00)")
                if "meffInc30_SRMMboostedWWEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*1.00+ (meffInc30>850.)*1.00)")
                if "meffInc30_SRHMboostedWWEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*1.00+ (meffInc30>850.)*0.67)")
                if "cuts_SRLMboostedWWdiscEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("1.00")
                if "cuts_SRMMboostedWWdiscEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("1.00")
                if "cuts_SRHMboostedWWdiscEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("1.00")
                if "meffInc30_SRLMboostedWZEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*1.00+ (meffInc30>850.)*1.00)")
                if "meffInc30_SRMMboostedWZEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*1.00+ (meffInc30>850.)*0.95)")
                if "meffInc30_SRHMboostedWZEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("( (meffInc30<850.)*1.00+ (meffInc30>850.)*1.00)")
                if "cuts_SRLMboostedWZdiscEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("0.50")
                if "cuts_SRMMboostedWZdiscEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("0.95")
                if "cuts_SRHMboostedWZdiscEM" == chan.name: 
                        chan.getSample("zjets_Sh_2211").addWeight("1.00")
    
            
    for chan in nonBTagChans[region]:
        chan.removeWeight("bTagWeight")
    
    for chan in ZTagChans[region]:
        chan.addWeight("fatjet1ZSF")

    for chan in WTagChans[region]:
        chan.addWeight("fatjet1WSF")
        

#########################MJ################################
'''
if doRemovezjetssystHMSR and myFitType == FitType.Background and myAna == "C1N2_WZ" and debug == False and doOnlySignal == False:
    for syst in basicChanSyst[region]:
        validation.getChannel("meffInc30", ["SRHMboostedWZEM"]).getSample("zjets_Sh_2211").removeSystematic(syst)
    validation.getChannel("meffInc30", ["SRHMboostedWZEM"]).getSample("zjets_Sh_2211").removeSystematic("zjets_Sh_2211_XS")
if doRemovezjetssystHMSR and myFitType == FitType.Exclusion and myAna == "C1N2_WZ" and debug == False and doOnlySignal == False:
    for syst in basicChanSyst[region]:
        myTopLvl.getChannel("meffInc30", ["SRHMboostedWZEM"]).getSample("zjets_Sh_2211").removeSystematic(syst)
    myTopLvl.getChannel("meffInc30", ["SRHMboostedWZEM"]).getSample("zjets_Sh_2211").removeSystematic("zjets_Sh_2211_XS")
'''
# ********************************************************************* #
#                              Plotting style
# ********************************************************************* #

c = ROOT.TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = ROOT.TLegend(0.55, 0.45, 0.87, 0.89, "") # without signal
#leg = ROOT.TLegend(0.55, 0.35, 0.87, 0.89, "") # with signal
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
#
entry = ROOT.TLegendEntry()
entry = leg.AddEntry("", "Data (#sqrt{s}=13 TeV)", "lp")
entry.SetMarkerColor(bkgOnly.dataColor)
entry.SetMarkerStyle(20)
#
entry = leg.AddEntry("", "Standard Model", "lf")
entry.SetLineColor(kBlack)
entry.SetLineWidth(4)
entry.SetFillColor(kBlue-5)
entry.SetFillStyle(3004)
#
entry = leg.AddEntry("", "t#bar{t}", "lf")
entry.SetLineColor(kGreen-9)
entry.SetFillColor(kGreen-9)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "W+jets", "lf")
entry.SetLineColor(kAzure-4)
entry.SetFillColor(kAzure-4)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "Diboson", "lf")
entry.SetLineColor(kOrange-8)
entry.SetFillColor(kOrange-8)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "Single Top", "lf")
entry.SetLineColor(kGreen-5)
entry.SetFillColor(kGreen-5)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "Z+jets", "lf")
entry.SetLineColor(kBlue+3)
entry.SetFillColor(kBlue+3)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "t#bar{t}V", "lf")
entry.SetLineColor(kGreen-8)
entry.SetFillColor(kGreen-8)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "t#bar{t}H", "lf")
entry.SetLineColor(kGreen-8)
entry.SetFillColor(kGreen-8)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "Multiboson", "lf")
entry.SetLineColor(kOrange-8)
entry.SetFillColor(kOrange-8)
entry.SetFillStyle(compFillStyle)
#
entry = leg.AddEntry("", "VH", "lf")
entry.SetLineColor(kGreen-8)
entry.SetFillColor(kGreen-8)
entry.SetFillStyle(compFillStyle)
#
#following lines if signal overlaid
#entry = leg.AddEntry("","10 x #tilde{g}#tilde{g} 1-step, m(#tilde{g}, #tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})=","l")
#entry = leg.AddEntry(""," ","l") 
#entry.SetLineColor(kMagenta)
#entry.SetLineStyle(kDashed)
#entry.SetLineWidth(4)
#
#entry = leg.AddEntry("","(1225, 625, 25) GeV","l") 
#entry.SetLineColor(kWhite)

# Set legend for TopLevelXML
bkgOnly.tLegend = leg
if validation:
    validation.totalPdfColor = kBlack
    #configMgr.plotRatio = "none" # AK: "none" is only for SR --> needs to be made part of ChannelStyle, not configMgr style
    validation.tLegend = leg

if myFitType==FitType.Exclusion:
    sig2 = sig.replace("p5", "").replace("p0", "")
    myTopLvl = configMgr.getFitConfig("Sig_%s"%sig2)
    myTopLvl.tLegend = leg
    myTopLvl.totalPdfColor = kBlack
    configMgr.plotRatio = "none"

c.Close()
MeffBins = ['1', '2', '3', '4']
# Plot "ATLAS" label
for chan in AllChannels_all:
    chan.titleY = "Entries"
    if not myFitType==FitType.Exclusion and not "SR" in chan.name: 
        chan.logY = True
    if chan.logY:
        chan.minY = 0.2
        chan.maxY = 50000
    chan.ATLASLabelX = 0.27 # AK: for CRs with ratio plot
    chan.ATLASLabelY = 0.83
    chan.ATLASLabelText = "Internal"
    chan.showLumi = True

if myFitType==FitType.Exclusion:
    for sig in sigSamples:
        for chan in SR_channels[sig]:
            chan.titleY = "Events"
            chan.minY = 0.05
            chan.maxY = 80
            chan.ATLASLabelX = 0.125
            chan.ATLASLabelY = 0.85
            chan. ATLASLabelText = "Internal"
            chan.showLumi = True

    configMgr.fitConfigs.remove(bkgOnly)

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName)


